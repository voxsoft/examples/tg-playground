# Terragrunt playground

This repo aims to provide a playground to experiment with [Terragrunt](https://github.com/gruntwork-io/terragrunt) - 
a thin wrapper for Terraform that provides extra tools for working with multiple Terraform modules,
remote state, and locking.

This repo contain a lot of examples and use cases of Terraform configurations and typical infrastructure patterns.
Some of them are WIP or abandoned, but it should give you an idea of a implementation pattern.

> **WARNING: This repo is not intended to be used as a production-ready infrastructure! It's just a playground to test!**

## Environments
| Full Name               | Typical Git Branch Name | Short Abbreviation  |
|:---                     |:---                     |:---                 |
|sandbox                  |sandbox                  |sb                   |
|development              |develop                  |de                   |
|quality assurance        |qa                       |qa                   |
|staging                  |stage                    |st                   |
|user acceptance testing  |uat                      |ut                   |
|production               |prod                     |pr                   |

Also `pr` abbreviation can be used as a `primer` environment. Initial environment to bootstrap the project.


## Directory structure

```sh
...
├── tf-gcp    ## GCP Infrastructure
│   ├── _modules      # GCP modules shared across all environments/projects
│   ├── utility       # Utility project to handle common resources (eg: Shared VPC, DNS, etc)
│   └── <GCP_PROJECT> # Name of the GCP Project=~Environment
├── tf-gitlab ## Gitlab Configuration
│   ├── _modules      # GitLab modules
│   └── users         # GitLab user management
...
```

## How to use it locally

### Prerequisites
- [Terraform](https://developer.hashicorp.com/terraform/downloads)
- [Terragrunt](https://terragrunt.gruntwork.io/docs/getting-started/install)
- [Gitlab Personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) (`api` scope)

### Init configuration
Since this repo is controlling a lot or resources, to run this configuration locally you need to
have access to the resource you want to control. 

#### [GCP](./tf-gcp/) Configuration
you need to have access to the GCP project you want to control, login to the GCP CLI,
and activate the [Default Application Credentials](https://cloud.google.com/docs/authentication/application-default-credentials).
```bash
gcloud auth login
gcloud auth application-default login
```

#### [Gitlab](./tf-gitlab/) Configuration
If you want to manage Gitlab configuration, you need to have Maintainer access to the Gitlab project/group you want to control.
See the Common requirements section to configure the Gitlab access token.

#### [Vault](./tf-vault/) Configuration
If you want to manage Vault configuration, you need to have access to the Vault server you want to control,
and provide your Vault token via environment variable.
```bash
export VAULT_TOKEN=<your_vault_token>
```

#### Common requirements to access the Terraform remote state:
```bash
export GITLAB_USER=<your_gitlab_username>
export GITLAB_TOKEN=<your_gitlab_access_token> ## To manage Gitlab
export CI_JOB_TOKEN=$GITLAB_TOKEN ## To access Gitlab Terraform state
terragrunt run-all plan
```
> TIP: You can add this config to your bash profile to avoid typing it every time

### Generate config for GitlabCI
To generate GitlabCI config for a project (required for new Terragrunt configurations): 
```bash
## Install the terragrunt-gitlab-cicd-config tool
go install github.com/kitos9112/terragrunt-gitlab-cicd-config@3.0.1-alpha
## Generate the config
terragrunt-gitlab-cicd-config generate --input .gitlab-ci.tpl.yaml
```
