# Gitlab Runner with docker-machine executor in OpenStack

## Pre-requisites
- Terraform
- Ansible
- OpenStack ~/.config/openstack/clouds.yaml file preconfigured, with credentials

## Configuration steps
1. In Gitlab, create a new Project/Group Runner in **CI/CD Settings** and specify:
    - Operating systems: select `Linux`
    - Tags, eg: `docker, docker-machine, openstack, lab, core`
    - Description, eg: `OpenStack lab, Autoscaling Docker Machine executor`
2. Store security-sensitive values in terraform.tfvars.
3. Run Terraform

## Useful commands
On Gitlab Runner host:
- `docker-machine create --driver openstack --help` Show configuration options for openstack 
- `docker-machine ls` List all docker machines
- `docker-machine rm -f $(docker-machine ls -q)` Remove all docker machines from machine. NOTE: If there are VM instances already created you must delete them manually.
- `journalctl -u gitlab-runner -f -n 10` Show logs of gitlab-runner service

## Additional resources
- Gitlab Runner advanced configuration: https://gitlab.com/gitlab-org/gitlab-runner/-/blob/main/docs/configuration/advanced-configuration.md
- Gitlab Runner autoscale config: https://docs.gitlab.com/runner/configuration/autoscale.html
- Docker-machine list configuration options for openstack: `docker-machine create --driver openstack --help`
