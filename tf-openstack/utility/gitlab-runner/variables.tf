variable "region" {
  description = "Default region"
  type        = string
  default     = "RegionOne"
}
variable "availability_zone" {
  description = "Default availability zone"
  type        = string
}
variable "network_name" {
  description = "Network name"
  type        = string
}
variable "gitlab_runner_token" {
  description = "Gitlab Runner token obtained from _CI/CD Settings > Runners_ page"
  type        = string
  sensitive   = true
}
variable "gitlab_runner_authorized_keys" {
  description = "Gitlab Runner additional authorized keys to be added to the instance"
  type        = list(string)
  default     = []
}
variable "gitlab_runner_openstack_username" {
  description = "Docker Machine - Openstack username to communicate with Openstack API"
  type        = string
}
variable "gitlab_runner_openstack_password" {
  description = "Docker Machine - Openstack password to communicate with Openstack API"
  type        = string
  sensitive   = true
}
variable "gitlab_runner_openstack_authurl" {
  description = "Docker Machine - Openstack auth url to communicate with Openstack API"
  type        = string
}
variable "gitlab_runner_openstack_domainname" {
  description = "Docker Machine - Openstack domain name to communicate with Openstack API"
  type        = string
}
