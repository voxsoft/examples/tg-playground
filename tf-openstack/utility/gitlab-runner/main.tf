terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.54"
    }
    ansible = {
      version = "~> 1.2"
      source  = "ansible/ansible"
    }
  }
}

provider "openstack" {} ## Configured `~/.config/openstack/clouds.yaml` expected

data "openstack_images_image_v2" "ubuntu" {
  name        = "Ubuntu Server 22.04 (Jammy Jellyfish)"
  most_recent = true
}

## TODO: Custom flavor for the docker-machine executors
# resource "openstack_compute_flavor_v2" "gitlab_runner" {
#   name      = "gl1.general.2c4gb"
#   ram       = "4096"
#   vcpus     = "2"
#   disk      = "50"
#   is_public = "false"
#   # extra_specs = {
#   #   "hw:cpu_policy"        = "CPU-POLICY",
#   #   "hw:cpu_thread_policy" = "CPU-THREAD-POLICY"
#   # }
# }

## Create SSH keypair for the instance and for docker-machine to use
resource "openstack_compute_keypair_v2" "gitlab_runner" {
  name = "gitlab-runner"
}

## Security group for the gitlab runner
resource "openstack_networking_secgroup_v2" "gitlab_runner" {
  name        = "gitlab-runner"
  description = "Gitlab Runners"
  region      = var.region
}
resource "openstack_networking_secgroup_rule_v2" "gitlab_runner_ssh" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.gitlab_runner.id
}
resource "openstack_networking_secgroup_rule_v2" "gitlab_runner_docker" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 2376
  port_range_max    = 2376
  remote_ip_prefix  = "${openstack_compute_instance_v2.gitlab_runner_0.access_ip_v4}/32"
  security_group_id = openstack_networking_secgroup_v2.gitlab_runner.id
}

## Pre-create a boot volume for instance
resource "openstack_blockstorage_volume_v3" "gitlab_runner_0" {
  name              = "gitlab-runner-0"
  region            = var.region
  availability_zone = var.availability_zone
  image_id          = data.openstack_images_image_v2.ubuntu.id
  size              = 20
}

resource "openstack_compute_instance_v2" "gitlab_runner_0" {
  name              = "gitlab-runner-0"
  region            = var.region
  availability_zone = var.availability_zone
  flavor_name       = "t1.general.2c4gb"
  block_device {
    uuid                  = openstack_blockstorage_volume_v3.gitlab_runner_0.id
    source_type           = "volume"
    destination_type      = "volume"
    boot_index            = 0
    delete_on_termination = true
  }
  key_pair        = openstack_compute_keypair_v2.gitlab_runner.name
  security_groups = [openstack_networking_secgroup_v2.gitlab_runner.name]
  network {
    name = var.network_name
  }
}

###~ Ansible ~###
## Create a local file with private key for the docker-machine
resource "local_sensitive_file" "gitlab_runner_private_key" {
  content  = openstack_compute_keypair_v2.gitlab_runner.private_key
  filename = "${path.module}/ansible/id_rsa"
}
## Create a local file with the extra authorized_keys
resource "local_file" "gitlab_runner_authorized_keys" {
  content  = join("\n", var.gitlab_runner_authorized_keys)
  filename = "${path.module}/ansible/authorized_keys"
}

resource "ansible_playbook" "gitlab_runner" {
  playbook   = "ansible/main.yaml"
  name       = openstack_compute_instance_v2.gitlab_runner_0.access_ip_v4
  replayable = true
  check_mode = false
  diff_mode  = true
  extra_vars = {
    ansible_user                             = data.openstack_images_image_v2.ubuntu.properties.os_distro
    ansible_ssh_private_key_file             = abspath(local_sensitive_file.gitlab_runner_private_key.filename)
    gitlab_runner_token                      = var.gitlab_runner_token
    gitlab_runner_openstack_username         = var.gitlab_runner_openstack_username
    gitlab_runner_openstack_password         = var.gitlab_runner_openstack_password
    gitlab_runner_openstack_tenantid         = openstack_networking_secgroup_v2.gitlab_runner.tenant_id
    gitlab_runner_openstack_network          = openstack_compute_instance_v2.gitlab_runner_0.network[0].name
    gitlab_runner_openstack_authurl          = var.gitlab_runner_openstack_authurl
    gitlab_runner_openstack_domainname       = var.gitlab_runner_openstack_domainname
    gitlab_runner_openstack_availabilityzone = var.availability_zone
  }
}

## Outputs
output "gitlab_runner_0" {
  value = { ip = openstack_compute_instance_v2.gitlab_runner_0.access_ip_v4 }
}
output "ansible_playbook_stdout" {
  value = ansible_playbook.gitlab_runner.ansible_playbook_stdout
}
