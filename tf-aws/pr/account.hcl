# Set account-wide variables. These are automatically pulled in to configure the remote state bucket in the root
# terragrunt.hcl configuration.
locals {
  account_name   = "voxmaster"
  aws_account_id = "257014405952"
  aws_profile    = "voxmaster"
  env_name       = "pr"
}
