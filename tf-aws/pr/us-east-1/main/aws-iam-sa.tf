###* AWS IAM | Service Accounts *###

# IAM service account for AWS ECR Read/Write Access
module "sa_ecr_rw" {
  source             = "../../../_modules/service-account"
  name               = "sa-ecr-rw"
  policy_attachments = ["arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryPowerUser"]
  key_create         = true
}
output "sa_ecr_rw_creds" {
  value = {
    AWS_REGION            = var.aws_region
    AWS_ACCESS_KEY_ID     = module.sa_ecr_rw.credentials.AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY = module.sa_ecr_rw.credentials.AWS_SECRET_ACCESS_KEY
  }
  description = "Use this AWS Credentials to login to ECR container registry"
  sensitive   = true
}
