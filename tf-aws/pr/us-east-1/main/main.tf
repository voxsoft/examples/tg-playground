data "aws_caller_identity" "current" {}
data "aws_availability_zones" "available" {}
module "key_pair" {
  source  = "terraform-aws-modules/key-pair/aws"
  version = "~> 2.0"

  key_name           = "sa-terraform"
  create_private_key = true
}
output "key_pair" {
  value = {
    private_key = module.key_pair.private_key_pem
    public_key = module.key_pair.public_key_pem
  }
  sensitive = true
}
