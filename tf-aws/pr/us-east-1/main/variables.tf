variable "env_name" {
  description = "Environment name prefix"
  default     = "pr"
  type        = string
}

variable "stop" {
  description = "Destroy all cost intensive resources"
  default     = true
  type        = bool
}

variable "aws_region" {
  description = "AWS region"
  default     = "us-east-1"
  type        = string
}

# variable "aws_zone" {
#   description = "Default AWS zone. Must be a valid zone in the region specified in aws_region"
#   default     = "us-east-1a"
#   type        = string
# }

variable "ecr_repos" {
  description = "List of ECR repositories to create"
  default     = ["py-timezones"]
  type        = list(string)
}

variable "vpc_cidr" {
  default     = "172.22.0.0/16"
  description = "VPC CIDR"
}

variable "eks_create" {
  description = "Create EKS cluster"
  default     = true
  type        = bool
}
variable "eks_cluster_name" {
  description = "Name of the EKS cluster"
  default     = "eks-cluster"
  type        = string
}
variable "eks_config" {
  description = "EKS cluster configuration"
  default     = {
    version = "1.27"
    enable_logs = false
    instance_types = ["t3.medium"]
    desired_size = 1
    max_size = 2
    min_size = 1
  }
}
