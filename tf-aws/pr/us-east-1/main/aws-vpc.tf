locals {
  vpc_subnets_number = 2 ## Per each type(public/private). 2...4 allowed in this implementation (EKS requires 2 AZ)
  vpc_azs   = slice(data.aws_availability_zones.default_region.names, 0, local.vpc_subnets_number)
  vpc_cidrs = {
    public  = slice([ for i in range(0, 4): cidrsubnet(var.vpc_cidr, 5, i) ], 0, local.vpc_subnets_number) # 2048 IPs per each subnet
    private = slice([ for i in range(4, 8): cidrsubnet(var.vpc_cidr, 5, i) ], 0, local.vpc_subnets_number) # 2048 IPs per each subnet
  }
}

data "aws_availability_zones" "default_region" {
  filter {
    name   = "region-name"
    values = [var.aws_region]
  }
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "~> 5.1.1"

  name = "main"
  cidr = var.vpc_cidr

  azs             = local.vpc_azs
  private_subnets = local.vpc_cidrs["private"]
  public_subnets  = local.vpc_cidrs["public"]

  enable_nat_gateway = var.stop ? false : true ## Enable for private cluster
  single_nat_gateway = true
  enable_vpn_gateway = false

  public_subnet_tags = {
    "kubernetes.io/role/elb" = 1
  }
  private_subnet_tags = {
    "kubernetes.io/role/internal-elb" = 1
    # Tags subnets for Karpenter auto-discovery
    "karpenter.sh/discovery" = var.eks_cluster_name
  }

  tags = {
    managed_by = "terraform"
  }
}

output "cidrs" {
  value = {
    private = local.vpc_cidrs["private"]
    public  = local.vpc_cidrs["public"]
    azs     = local.vpc_azs
  }
}
