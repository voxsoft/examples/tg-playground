#* AWS ECR for projects *#

module "ecr" {
  for_each = toset(var.ecr_repos)
  source   = "terraform-aws-modules/ecr/aws"
  version  = "~> 1.6.0"

  repository_name                 = each.key
  repository_image_tag_mutability = "MUTABLE"
  repository_image_scan_on_push   = false
  repository_lifecycle_policy     = local.ecr_policy_default

  tags = {
    managed_by = "terraform"
  }
}


locals {
  ecr_policy_default = jsonencode({
    rules = [
      {
        "rulePriority" = 10
        "description"  = "Base"
        "selection" = {
          "tagStatus"     = "tagged"
          "tagPrefixList" = ["base", "parent"]
          "countType"     = "imageCountMoreThan"
          "countNumber"   = 3
        }
        "action" = {
          "type" = "expire"
        }
      },

      {
        "rulePriority" = 20
        "description"  = "v*"
        "selection" = {
          "tagStatus"     = "tagged"
          "tagPrefixList" = ["v0", "v1", "v2", "v3", "v4", "v5", "v6", "v7", "v8", "v9"]
          "countType"     = "imageCountMoreThan"
          "countNumber"   = 10
        }
        "action" = {
          "type" = "expire"
        }
      },

      {
        "rulePriority" = 30
        "description"  = "Production"
        "selection" = {
          "tagStatus"     = "tagged"
          "tagPrefixList" = ["release", "prod", "master", "hotfix"]
          "countType"     = "imageCountMoreThan"
          "countNumber"   = 10
        }
        "action" = {
          "type" = "expire"
        }
      },

      {
        "rulePriority" = 40
        "description"  = "Development"
        "selection" = {
          "tagStatus"     = "tagged"
          "tagPrefixList" = ["dev", "feature"]
          "countType"     = "imageCountMoreThan"
          "countNumber"   = 5
        }
        "action" = {
          "type" = "expire"
        }
      },

      {
        "rulePriority" = 50
        "description"  = "Staging"
        "selection" = {
          "tagStatus"     = "tagged"
          "tagPrefixList" = ["staging"]
          "countType"     = "imageCountMoreThan"
          "countNumber"   = 5
        }
        "action" = {
          "type" = "expire"
        }
      },

      {
        "rulePriority" = 60
        "description"  = "UAT"
        "selection" = {
          "tagStatus"     = "tagged"
          "tagPrefixList" = ["uat"]
          "countType"     = "imageCountMoreThan"
          "countNumber"   = 5
        }
        "action" = {
          "type" = "expire"
        }
      },

      {
        "rulePriority" = 70
        "description"  = "Maintenance"
        "selection" = {
          "tagStatus"     = "tagged"
          "tagPrefixList" = ["ci-fix"]
          "countType"     = "imageCountMoreThan"
          "countNumber"   = 5
        }
        "action" = {
          "type" = "expire"
        }
      },

      {
        "rulePriority" = 80
        "description"  = "Untagged"
        "selection" = {
          "tagStatus"   = "untagged"
          "countType"   = "imageCountMoreThan"
          "countNumber" = 1
        }
        "action" = {
          "type" = "expire"
        }
      },

      {
        "rulePriority" = 90
        "description"  = "Other"
        "selection" = {
          "tagStatus"   = "any"
          "countType"   = "imageCountMoreThan"
          "countNumber" = 200
        }
        "action" = {
          "type" = "expire"
        }
      },
    ]
  })


}
