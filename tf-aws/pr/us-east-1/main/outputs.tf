output "ecr_repos_urls" {
  description = "List of ECR repositories created"
  value       = [for repo in module.ecr : repo.repository_url]
}
