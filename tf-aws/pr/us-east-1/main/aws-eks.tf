###* EKS *###
locals {
  eks_logging = var.eks_config.enable_logs ? {
    types   = ["api", "audit", "authenticator"]
    enabled = true
  } : {
    types   = []
    enabled = false
  }
}

module "eks" {
  count   = var.eks_create && !var.stop ? 1 : 0
  source  = "terraform-aws-modules/eks/aws"
  version = "~> 19.16.0"

  cluster_name    = var.eks_cluster_name
  cluster_version = lookup(var.eks_config, "version", "1.27")

  vpc_id     = module.vpc.vpc_id
  subnet_ids = module.vpc.private_subnets
  # control_plane_subnet_ids = ???
  cluster_endpoint_public_access = true

  manage_aws_auth_configmap = false
  enable_irsa               = true
  ## Cloudwatch EKS logs
  create_cloudwatch_log_group = local.eks_logging.enabled
  cluster_enabled_log_types   = local.eks_logging.types

  tags = { managed_by = "terraform" }

  eks_managed_node_groups = {
    apps = {
      name            = "eks-apps"
      use_name_prefix = true
      key_name        = module.key_pair.key_pair_name

      subnet_ids = slice(module.vpc.private_subnets, 0, 1)

      instance_types          = lookup(var.eks_config, "instance_types", ["t3.medium"])
      capacity_type           = "SPOT" #"ON_DEMAND"
      force_update_version    = true
      ebs_optimized           = true
      disable_api_termination = false
      enable_monitoring       = false

      min_size     = lookup(var.eks_config, "min_size", 1)
      max_size     = lookup(var.eks_config, "max_size", 2)
      desired_size = lookup(var.eks_config, "desired_size", 1)

      block_device_mappings = {
        xvda = {
          device_name = "/dev/xvda"
          ebs = {
            volume_size = 35
            volume_type = "gp3"
            # iops                  = 3000
            # throughput            = 150
            # encrypted             = false
            # kms_key_id            = 
            delete_on_termination = true
          }
        }
      }

      labels = {
        workload-type = "application" # Represent type of workload, required (eg: application, maintenance, monitoring, gitlab-runner)
        workload-env  = var.env_name
      }
    }
  }
}
