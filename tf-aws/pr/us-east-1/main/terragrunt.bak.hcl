## Terraform Module
# Local

## Inlcudes
include "root" { # Root level config
  path = find_in_parent_folders("root.hcl")
}
include "aws" { # GCP level config
  path = find_in_parent_folders("aws.hcl")
}

## Providers
generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "aws" { region = "${local.region.aws_region}" }
EOF
}

## Locals and inputs
locals {
  account = read_terragrunt_config(find_in_parent_folders("account.hcl")).locals
  region  = read_terragrunt_config(find_in_parent_folders("region.hcl")).locals
}
inputs = {
  env_name             = local.account.env_name
  aws_region           = local.region.aws_region
  aws_zone             = local.region.aws_zone
  // resource_name_prefix = "voxmaster"

  eks_cluster_name     = "voxmaster"
}
