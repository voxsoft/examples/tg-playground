
###* Terraform backend configuration *###

terraform {
  required_version = ">= 1.0"
  required_providers {}
  # backend "gcs" {
  #   # credentials = "./sa-terraform.json"
  #   bucket      = "terraform-state"
  #   # prefix      = "terraform/.pitfall" #! Incorrect terraform initializations will be stored here. 
  #   #                                    #! If you see "Unsupported state file format" error. Your initialization is incorrect!
  #   prefix      = "terraform/develop/aws"
  #   # encryption_key = "32 byte base64 encoded"
  # }
}

###* Main provider *###
provider "aws" {
  # shared_credentials_file = "$HOME/.aws/credentials"
  region                  = "us-east-1"
}
data "aws_caller_identity" "current" {}
