###* VPC Configuration *###

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "my-vpc"
  cidr = "10.0.0.0/16"

  azs             = ["us-east-1a", "us-east-1b", "us-east-1c"]
  # private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_nat_gateway = false
  enable_vpn_gateway = false

  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}


data "aws_availability_zones" "main" {
  filter {
    name   = "region-name"
    values = ["us-east-1"]
  }
}


# resource "aws_vpc" "main" {
#   cidr_block           = var.main_vpc_ip_range
#   enable_dns_hostnames = true
#   enable_dns_support   = true
#   tags = {
#     "Name"       = "Main VPC",
#     "managed_by" = "terraform",
#     # "kubernetes.io/cluster/${var.eks_0_cluster_name}" = "shared",
#   }
# }

# resource "aws_internet_gateway" "main_internet_gateway" {
#   vpc_id = aws_vpc.main.id
#   tags = {
#     "Name"       = "Main VPC Internet Gateway"
#     "managed_by" = "terraform"
#   }
# }

# # Elastic IP for PROD NAT gateway
# resource "aws_eip" "nat_eip_00" {
#   vpc        = true
#   depends_on = [aws_internet_gateway.main_internet_gateway]
# }
# # Elastic IP for NON-PROD NAT gateway
# resource "aws_eip" "nat_eip_10" {
#   vpc        = true
#   depends_on = [aws_internet_gateway.main_internet_gateway]
# }

# # PRODUCTION NAT gateway
# resource "aws_nat_gateway" "nat_gateway_00" {
#   allocation_id = aws_eip.nat_eip_00.id
#   subnet_id     = aws_subnet.eks_0_subnet.0.id
#   depends_on    = [aws_subnet.eks_0_subnet]
#   tags = {
#     "Name"        = "artstation-prod-nat-gw"
#     "managed_by"  = "terraform"
#   }
# }

# # NON-PRODUCTION NAT gateway
# resource "aws_nat_gateway" "nat_gateway_10" {
#   allocation_id = aws_eip.nat_eip_10.id
#   subnet_id     = aws_subnet.eks_1_subnet.0.id
#   depends_on    = [aws_subnet.eks_1_subnet]
#   tags = {
#     "Name"        = "artstation-nonprod-nat-gw"
#     "managed_by"  = "terraform"
#   }
# }

# resource "aws_route_table" "main_route_table" {
#   vpc_id = aws_vpc.main.id
#   # Internet Gateway
#   route {
#     cidr_block = "0.0.0.0/0"
#     gateway_id = aws_internet_gateway.main_internet_gateway.id
#   }
#   tags = {
#     "managed_by" = "terraform"
#   }
# }

# # Routing table for PRODUCTION private subnets
# resource "aws_route_table" "eks_00_route_table" {
#   vpc_id = aws_vpc.main.id
#   route {
#     cidr_block = "0.0.0.0/0"
#     nat_gateway_id = aws_nat_gateway.nat_gateway_00.id
#   }
#   tags = {
#     "managed_by"  = "terraform"
#     "environment" = "production"
#   }
# }
# # Routing table for NON-PRODUCTION private subnets
# resource "aws_route_table" "eks_10_route_table" {
#   vpc_id = aws_vpc.main.id
#   route {
#     cidr_block = "0.0.0.0/0"
#     nat_gateway_id = aws_nat_gateway.nat_gateway_10.id
#   }
#   tags = {
#     "managed_by"  = "terraform"
#     "environment" = "nonproduction"
#   }
# }


# # Subnets for EKS-0
# resource "aws_subnet" "eks_0_subnet" {
#   count             = 3
#   availability_zone = data.aws_availability_zones.main.names[count.index]
#   cidr_block        = cidrsubnet(aws_vpc.main.cidr_block, 5, count.index) # 2048 IPs per each subnet
#   vpc_id            = aws_vpc.main.id

#   map_public_ip_on_launch = true
#   tags = {
#     "Name"                                            = "EKS-0 Subnet ${data.aws_availability_zones.main.names[count.index]}",
#     "managed_by"                                      = "terraform",
#     "kubernetes.io/cluster/${var.eks_0_config.name}"  = "shared",
#     "kubernetes.io/cluster/${var.eks_00_config.name}" = "shared",
#     "kubernetes.io/role/elb"                          = "",
#   }
# }

# resource "aws_route_table_association" "eks_0_route_table_association" {
#   count          = 3
#   subnet_id      = aws_subnet.eks_0_subnet.*.id[count.index]
#   route_table_id = aws_route_table.main_route_table.id
# }

# #Private Subnets for EKS-00
# resource "aws_subnet" "eks_00_subnet" {
#   count             = 3
#   availability_zone = data.aws_availability_zones.main.names[count.index]
#   cidr_block        = cidrsubnet(aws_vpc.main.cidr_block, 5, count.index + 3) # 2048 IPs per each subnet
#   vpc_id            = aws_vpc.main.id

#   map_public_ip_on_launch = true
#   tags = {
#     "Name"                                           = "EKS-00 Subnet ${data.aws_availability_zones.main.names[count.index]} PRIVATE",
#     "managed_by"                                     = "terraform",
#     "kubernetes.io/cluster/${var.eks_00_config.name}" = "shared",
#   }
# }

# resource "aws_route_table_association" "eks_00_route_table_association" {
#   count          = 3
#   subnet_id      = aws_subnet.eks_00_subnet.*.id[count.index]
#   route_table_id = aws_route_table.eks_00_route_table.id
# }

# /*
# # Subnets for Other EC2 instances
# resource "aws_subnet" "ec2_00_subnet" {
#   count                   = 3
#   availability_zone       = data.aws_availability_zones.main.names[count.index]
#   cidr_block              = cidrsubnet(aws_vpc.main.cidr_block, 5, count.index + 6) # 2048 IPs per each subnet
#   vpc_id                  = aws_vpc.main.id
#   map_public_ip_on_launch = true
#   tags = {
#     "Name"       = "EC2 Subnet ${data.aws_availability_zones.main.names[count.index]}",
#     "managed_by" = "terraform",
#   }
# }

# resource "aws_route_table_association" "ec2_00_route_table_association" {
#   count          = 3
#   subnet_id      = aws_subnet.ec2_00_subnet.*.id[count.index]
#   route_table_id = aws_route_table.main_route_table.id
# }
# */

# # Subnets for EKS-1
# resource "aws_subnet" "eks_1_subnet" {
#   count             = 3
#   availability_zone = data.aws_availability_zones.main.names[count.index]
#   cidr_block        = cidrsubnet(aws_vpc.main.cidr_block, 5, count.index + 16) # 2048 IPs per each subnet
#   vpc_id            = aws_vpc.main.id

#   map_public_ip_on_launch = true
#   tags = {
#     "Name"                                            = "EKS-1 Subnet ${data.aws_availability_zones.main.names[count.index]}",
#     "managed_by"                                      = "terraform",
#     "kubernetes.io/cluster/${var.eks_1_config.name}"  = "shared",
#     "kubernetes.io/cluster/${var.eks_10_config.name}" = "shared",
#     "kubernetes.io/role/elb"                          = "",
#   }
# }

# resource "aws_route_table_association" "eks_1_route_table_association" {
#   count          = 3
#   subnet_id      = aws_subnet.eks_1_subnet.*.id[count.index]
#   route_table_id = aws_route_table.main_route_table.id
# }

# #Private Subnets for EKS-10
# resource "aws_subnet" "eks_10_subnet" {
#   count             = 3
#   availability_zone = data.aws_availability_zones.main.names[count.index]
#   cidr_block        = cidrsubnet(aws_vpc.main.cidr_block, 5, count.index + 19) # 2048 IPs per each subnet
#   vpc_id            = aws_vpc.main.id

#   map_public_ip_on_launch = true
#   tags = {
#     "Name"                                           = "EKS-10 Subnet ${data.aws_availability_zones.main.names[count.index]} PRIVATE",
#     "managed_by"                                     = "terraform",
#     "kubernetes.io/cluster/${var.eks_10_config.name}" = "shared",
#   }
# }

# resource "aws_route_table_association" "eks_10_route_table_association" {
#   count          = 3
#   subnet_id      = aws_subnet.eks_10_subnet.*.id[count.index]
#   route_table_id = aws_route_table.eks_10_route_table.id
# }

# # Subnets for Other EC2 instances
# resource "aws_subnet" "ec2_10_subnet" {
#   count                   = 3
#   availability_zone       = data.aws_availability_zones.main.names[count.index]
#   cidr_block              = cidrsubnet(aws_vpc.main.cidr_block, 5, count.index + 24) # 2048 IPs per each subnet
#   vpc_id                  = aws_vpc.main.id
#   map_public_ip_on_launch = true
#   tags = {
#     "Name"       = "EC2 Subnet ${data.aws_availability_zones.main.names[count.index]}",
#     "managed_by" = "terraform",
#   }
# }

# resource "aws_route_table_association" "ec2_10_route_table_association" {
#   count          = 3
#   subnet_id      = aws_subnet.ec2_10_subnet.*.id[count.index]
#   route_table_id = aws_route_table.main_route_table.id
# }

