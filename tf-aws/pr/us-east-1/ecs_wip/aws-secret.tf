resource "aws_secretsmanager_secret" "app_gcp_sa_key_json" {
  name = "app_gcp_sa_key_json"
}

resource "aws_secretsmanager_secret_version" "app_gcp_sa_key_json" {
  secret_id     = aws_secretsmanager_secret.app_gcp_sa_key_json.id
  secret_string = file("./sa-json.json")
}
