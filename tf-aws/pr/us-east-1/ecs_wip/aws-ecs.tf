module "ecs" {
  source = "terraform-aws-modules/ecs/aws"

  cluster_name = "ecs-fargate"

  cluster_configuration = {
    execute_command_configuration = {
      logging = "NONE"
      # log_configuration = {
      #   cloud_watch_log_group_name = "/aws/ecs/aws-ec2"
      # }
    }
  }

  fargate_capacity_providers = {
    # FARGATE = {
    #   default_capacity_provider_strategy = {
    #     base = 1
    #     weight = 1
    #   }
    # }
    FARGATE_SPOT = {
      default_capacity_provider_strategy = {
        base   = 1
        weight = 1
      }
    }
  }

  tags = {
    Environment = "Development"
    Project     = "EcsEc2"
  }
}


####?
resource "aws_cloudwatch_log_group" "this" {
  name_prefix       = "hello_world-"
  retention_in_days = 1
}

resource "aws_ecs_task_definition" "this" {
  family                   = "hello_world"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn

  container_definitions = jsonencode([
    ## ESP
    {
      name  = "esp"
      image = "gcr.io/endpoints-release/endpoints-runtime:2"

      entryPoint = [
        "/bin/sh",
        "-c",
        "echo $GCP_SERVICE_ACCOUNT_KEY_JSON > $GOOGLE_APPLICATION_CREDENTIALS && python3 /apiproxy/start_proxy.py --non_gcp --listener_port=8081 --backend=localhost:80 --service=app-endpoint.voxsoft.pro --rollout_strategy=managed --healthz=/healthz --cors_preset=basic --cors_allow_origin=app-endpoint.voxsoft.pro"
      ]

      /*
      entryPoint = [
        "python3",
        "/apiproxy/start_proxy.py"
      ]
      args = [
        "--listener_port=8081",
        "--backend=127.0.0.1:80",
        "--service=app-endpoint.voxsoft.pro",
        "--rollout_strategy=managed",
        "--healthz=/healthz",
        "--cors_preset=basic",
        "--cors_allow_origin=app-endpoint.voxsoft.pro"
      ]
      */
      secrets = [
        {
          name      = "GCP_SERVICE_ACCOUNT_KEY_JSON"
          valueFrom = aws_secretsmanager_secret.app_gcp_sa_key_json.arn
        }
      ]
      environment = [
        {
          name  = "GOOGLE_APPLICATION_CREDENTIALS"
          value = "/tmp/service_account_key.json"
        }
      ]
      portMappings = [
        {
          name          = "esp"
          containerPort = 8081
        }
      ]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          "awslogs-region" = "us-east-1"
          "awslogs-group"  = aws_cloudwatch_log_group.this.name
          # "awslogs-create-group" = "true" 
          "awslogs-stream-prefix" = "esp"
        }
      }
    },
    ## APP
    {
      name   = "hello_world"
      image  = "nginx:stable"
      cpu    = 32
      memory = 32
      portMappings = [
        {
          name          = "http"
          containerPort = 80
          # hostPort      = 80
          # protocol      = "tcp"
        }
      ]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          "awslogs-region" = "us-east-1"
          "awslogs-group"  = aws_cloudwatch_log_group.this.name
          # "awslogs-create-group" = "true" 
          "awslogs-stream-prefix" = "ecs"
        }
      }
    },

  ])
}



/*
resource "aws_ecs_service" "this" {
  name            = "hello_world"
  cluster         = module.ecs.cluster_id
  task_definition = aws_ecs_task_definition.this.arn
  network_configuration {
    subnets          = [module.vpc.public_subnets[0]]
    security_groups  = [module.ecs_hello_sg.security_group_id]
    assign_public_ip = true
  }

  desired_count = 1

  deployment_maximum_percent         = 100
  deployment_minimum_healthy_percent = 0

  lifecycle {
    ignore_changes = [
      desired_count, capacity_provider_strategy
    ]
  }

  # load_balancer {
  #   target_group_arn = module.alb.target_group_arns[0]
  #   container_name   = "hello_world"
  #   container_port   = 80
  # }
}
# /*

####?
module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 8.0"

  name = "my-alb"

  load_balancer_type = "application"

  vpc_id          = module.vpc.vpc_id
  subnets         = [module.vpc.public_subnets[0], module.vpc.public_subnets[1], module.vpc.public_subnets[2]]
  security_groups = [module.ecs_hello_sg.security_group_id]

  # access_logs = {
  #   bucket = "my-alb-logs"
  # }

  target_groups = [
    {
      name_prefix      = "pref-"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = "ip"
      targets          = {}
    }
  ]

  # https_listeners = [
  #   {
  #     port               = 443
  #     protocol           = "HTTPS"
  #     certificate_arn    = "arn:aws:iam::123456789012:server-certificate/test_cert-123456789012"
  #     target_group_index = 0
  #   }
  # ]

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      target_group_index = 0
    }
  ]

  tags = {
    Environment = "Test"
  }
}

*/


module "ecs_hello_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "hello"
  description = "Security group for user-service with custom ports open within VPC, and PostgreSQL publicly open"
  vpc_id      = module.vpc.vpc_id

  # ingress_cidr_blocks      = ["10.10.0.0/16"]
  # egress_cidr_blocks = ["0.0.0.0/0"]
  egress_rules        = ["all-all"]
  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["https-443-tcp", "http-80-tcp" ]
  #?
  # computed_ingress_rules = ["http-8081-tcp"]
  # number_of_computed_ingress_rules = 1
  # ingress_with_cidr_blocks = [
  #   {
  #     from_port   = 80
  #     to_port     = 80
  #     protocol    = "tcp"
  #     description = "User-service ports"
  #     cidr_blocks = "10.10.0.0/16"
  #   },
  #   {
  #     rule        = "postgresql-tcp"
  #     cidr_blocks = "0.0.0.0/0"
  #   },
  # ]
}


#logs:CreateLogStream and logs:PutLogEvents are required for the log driver to work. The IAM role for the task needs to have these permissions.
resource "aws_iam_role" "ecs_task_execution_role" {
  name = "ecsTaskExecutionRole"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        }
      },
    ]
  })
}
resource "aws_iam_role_policy_attachment" "ecs_task_execution_role_attachment" {
  role       = aws_iam_role.ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}
resource "aws_iam_role_policy_attachment" "ecs_task_execution_role_attachment_secrets_manager" {
  # NOTE: More restriction: https://aws.amazon.com/premiumsupport/knowledge-center/ecs-data-security-container-task/
  role       = aws_iam_role.ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/SecretsManagerReadWrite"
}
