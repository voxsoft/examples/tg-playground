resource "aws_iam_user" "this" {
  name = var.name
  tags = {
    managed_by = "terraform"
  }
}
resource "aws_iam_user_policy_attachment" "AmazonEC2ContainerRegistryPowerUser" {
  for_each   = toset(var.policy_attachments)
  user       = aws_iam_user.this.name
  policy_arn = each.key
}

resource "aws_iam_access_key" "this" {
  count      = var.key_create ? 1 : 0
  user       = aws_iam_user.this.name
  status     = var.key_status
  ## TODO: Key rotation mechanism
  # for_each   = var.key_create ? tomap([random_id.access_key_serial[0].hex]) : {}
  # depends_on = [ random_id.access_key_serial ]
}
## TODO: Key rotation mechanism
# resource "random_id" "access_key_serial" {
#   count       = var.key_create ? 1 : 0
#   keepers     = var.key_rotation_days > 0 ? { rotation_time = time_rotating.sa_key_rotation[0].rotation_rfc3339 } : {}
#   byte_length = "2"
# }
# resource "time_rotating" "sa_key_rotation" {
#   count         = var.key_create && var.key_rotation_days > 0 ? 1 : 0
#   rotation_days = var.key_rotation_days
# }


## Outputs
output "name" {
  value       = aws_iam_user.this.name
  description = "Service Account Name"
}
output "credentials" {
  value = var.key_create ? {
    AWS_ACCESS_KEY_ID     = aws_iam_access_key.this[0].id
    AWS_SECRET_ACCESS_KEY = aws_iam_access_key.this[0].secret
  } : {}
  sensitive   = true
  description = "Use this AWS Credentials to login to ECR container registry"
}

