variable "name" {
  type        = string
  description = "Service Account Name to create"
}
variable "key_create" {
  type        = bool
  description = "Create Service Account Programmatic Access Key"
  default     = false
}
variable "key_status" {
  type        = string
  description = "Service Account Programmatic Access Key Status. Valid values are `Active` or `Inactive`"
  default     = "Active"
}
variable "key_rotation_days" {
  type        = number
  description = "Service Account Programmatic Access Key Rotation Days. Set `0` to disable rotation."
  default     = 0
}
variable "policy_attachments" {
  type        = list(string)
  description = "List of IAM Policy ARNs to attach to the Service Account"
  default     = []
}
