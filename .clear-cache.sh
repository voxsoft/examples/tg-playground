#!/usr/bin/env bash
find . -type d -name ".terragrunt-cache" -prune -exec rm -rf {} \;
find . -type d -name ".terraform" -prune -exec rm -rf {} \;
find . -type f -name ".terraform.lock.hcl" -prune -exec rm -rf {} \;
find . -type f -name "_backend.tf" -prune -exec rm -rf {} \;
find . -type f -name "backend.tf" -prune -exec rm -rf {} \;
find . -type f -name "_provider.tf" -prune -exec rm -rf {} \;
find . -type f -name "provider.tf" -prune -exec rm -rf {} \;
find . -type f -name "plan.bin" -prune -exec rm -rf {} \;
find . -type f -name "plan.json" -prune -exec rm -rf {} \;
