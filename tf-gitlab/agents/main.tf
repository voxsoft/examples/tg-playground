terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}

data "gitlab_project" "this" {
  path_with_namespace = "voxsoft/beerit/beerit-ci"
}

resource "gitlab_cluster_agent" "this" {
  project = data.gitlab_project.this.id
  name    = "beerit-pr"
}

resource "gitlab_cluster_agent_token" "this" {
  project     = data.gitlab_project.this.id
  agent_id    = gitlab_cluster_agent.this.agent_id
  name        = "beerit-pr"
  description = "Token for the `beerit-pr` used with `gitlab-agent` Helm Chart"
}

output "gitlab_cluster_agent_token" {
  sensitive = true
  value     = gitlab_cluster_agent_token.this.token
}
