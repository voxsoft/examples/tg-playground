## Inlcudes
include "root" { # Root level config
  path = find_in_parent_folders("root.hcl")
}
include "gitlab" { # Gitlab level config
  path = find_in_parent_folders("gitlab.hcl")
}

## Inputs
