## Inlcudes
include "root" { # Root level config
  path = find_in_parent_folders("root.hcl")
}
include "gitlab" { # Gitlab level config
  path = find_in_parent_folders("gitlab.hcl")
}

## Dependenies, locals and inputs
dependency "gcp_utility_sa" {
  config_path = "${get_path_to_repo_root()}/tf-gcp/utility/main"
  mock_outputs = {
    sa_terraform_key_b64 = ""
  }
}

## Inputs
inputs = {
  terraform_sa_key_b64 = dependency.gcp_utility_sa.outputs.sa_terraform_key_b64
}
