terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}

variable "terraform_sa_key_b64" {
  type      = string
  sensitive = true
  default   = null
}

data "gitlab_project" "tg-playground" {
  path_with_namespace = "voxsoft/examples/tg-playground"
}
resource "gitlab_project_variable" "terraform_sa_key_b64" {
  count     = var.terraform_sa_key_b64 != null ? 1 : 0
  project   = data.gitlab_project.tg-playground.id
  key       = "GCP_SA_TF_KEY_B64"
  value     = var.terraform_sa_key_b64
  protected = false
  raw       = true
  masked    = true
}
