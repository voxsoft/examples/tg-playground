###* Gitlab provider *###

variable "default_expires_at" {
  description = "Default expiration date for membership, if not defined in users_permissions. Format: <YYYY-MM-DD> . Leave blank to not set"
  default     = null
}
terraform {
  required_version = ">= 1.0"
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}



###* MAIN RESOURCES *###
locals {
  ## Load users_permissions from YAML
  users_permissions = yamldecode(file("users-permissions.yaml"))
  ## Users List
  users_list = [for k, v in local.users_permissions : k]

  ## Intermediate groups value
  groups_obj = merge([for user in local.users_permissions : user.groups if can(user.groups)]...)
  ## Groups list
  groups_list = [for k, v in local.groups_obj : k]
  ## Intermediate user/groups value
  groups_in_users = { for k, v in local.users_permissions : k => [for ka, ve in local.users_permissions[k].groups : ka] if can(v.groups) }
  ## Combined users+groups vale: <USERNAME>/<GROUP_PATH>
  groups_in_users_list = flatten([for k, v in local.groups_in_users : formatlist("${k}/%s", v)])

  ## Intermediate projects value
  projects_obj = merge([for user in local.users_permissions : user.projects if can(user.projects)]...)
  ## Projects list
  projects_list = [for k, v in local.projects_obj : k]
  ## Intermediate user/projects value
  projects_in_users = { for k, v in local.users_permissions : k => [for ka, ve in local.users_permissions[k].projects : ka] if can(v.projects) }
  ## Combined users+projects vale: <USERNAME>/<PROJECT_PATH>
  projects_in_users_list = flatten([for k, v in local.projects_in_users : formatlist("${k}/%s", v)])


}

data "gitlab_user" "this" {
  for_each = toset(local.users_list)
  username = each.key
}
data "gitlab_group" "this" {
  for_each  = toset(local.groups_list)
  full_path = each.key
}
data "gitlab_project" "this" {
  for_each            = toset(local.projects_list)
  path_with_namespace = each.key
}

resource "gitlab_group_membership" "this" {
  depends_on   = [data.gitlab_group.this, data.gitlab_user.this]
  for_each     = toset(local.groups_in_users_list)
  user_id      = data.gitlab_user.this[split("/", each.key).0].id
  group_id     = data.gitlab_group.this[trimprefix(each.key, "${split("/", each.key).0}/")].id
  access_level = local.users_permissions[split("/", each.key).0].groups[trimprefix(each.key, "${split("/", each.key).0}/")].role
  expires_at = lookup(
    local.users_permissions[split("/", each.key).0].groups[trimprefix(each.key, "${split("/", each.key).0}/")], "expires_at", null
    ) == null ? var.default_expires_at : lookup(
    local.users_permissions[split("/", each.key).0].groups[trimprefix(each.key, "${split("/", each.key).0}/")], "expires_at", null
  )
}

resource "gitlab_project_membership" "this" {
  depends_on   = [data.gitlab_project.this, data.gitlab_user.this]
  for_each     = toset(local.projects_in_users_list)
  user_id      = data.gitlab_user.this[split("/", each.key).0].id
  project      = data.gitlab_project.this[trimprefix(each.key, "${split("/", each.key).0}/")].id
  access_level = local.users_permissions[split("/", each.key).0].projects[trimprefix(each.key, "${split("/", each.key).0}/")].role
  expires_at = lookup(
    local.users_permissions[split("/", each.key).0].projects[trimprefix(each.key, "${split("/", each.key).0}/")], "expires_at", null
    ) == null ? var.default_expires_at : lookup(
    local.users_permissions[split("/", each.key).0].projects[trimprefix(each.key, "${split("/", each.key).0}/")], "expires_at", null
  )
}

###* OUTPUTS *###
/*
## Debug
output "DUMP" {
  value = local.users_permissions
}
output "USERS" {
  value = local.users_list
}
output "GROUPS" {
  value = local.groups_list
}
output "PROJECTS" {
  value = local.projects_list
}
*/
output "MEMBERS_GROUPS" {
  value = local.groups_in_users_list
}
output "MEMBERS_PROJECTS" {
  value = local.projects_in_users_list
}
