generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "gitlab" { token = "${get_env("GITLAB_TOKEN")}" }
EOF
}
