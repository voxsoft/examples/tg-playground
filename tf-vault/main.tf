## Structure: TBD?
## Where we should keep Common secrets?
##
## apps/<group>/<project>/<environment(sb|de|qa|st|ut|pr)>/<env-specific-secret-name>
## apps/<group>/<project>/<common-secret-name>

resource "vault_mount" "kvv2" {
  path        = "apps"
  type        = "kv"
  options     = { version = "2" }
  description = "KV Version 2 secret engine mount"
}

resource "vault_kv_secret_backend_v2" "kvv2" {
  mount = vault_mount.kvv2.path
  # max_versions         = 5
  # delete_version_after = 12600
  # cas_required         = true
}


resource "vault_kv_secret_v2" "app_environment_specific_secret" {
  mount               = vault_mount.kvv2.path
  name                = "groupA/projectB/develop/environment_variables"
  cas                 = 1
  delete_all_versions = true
  data_json = jsonencode({
    zip = "zap",
    foo = "foos"
  })
  custom_metadata {
    max_versions = 5
    data = {
      description = "App Environment-specific Secret",
      owner       = "somebody@example.com"
    }
  }
}

resource "vault_kv_secret_v2" "app_common_secret" {
  mount               = vault_mount.kvv2.path
  name                = "groupA/projectB/environment_variables"
  cas                 = 1
  delete_all_versions = true
  data_json = jsonencode({
    zip = "zap",
    foo = "foos"
  })
  custom_metadata {
    max_versions = 5
    data = {
      description = "This is a sample secret",
      owner       = "somebody@example.com"
    }
  }
}

resource "vault_kv_secret_v2" "group_common_secret" {
  mount               = vault_mount.kvv2.path
  name                = "groupA/environment_variables"
  cas                 = 1
  delete_all_versions = true
  data_json = jsonencode({
    zip = "zap",
    foo = "foos"
  })
  custom_metadata {
    max_versions = 5
    data = {
      description = "This is a sample secret",
      owner       = "somebody@example.com"
    }
  }
}

### Gitlab example https://gitlab.com/voxmaster/test/-/tree/vault
resource "vault_kv_secret_v2" "project_develop_secret" {
  mount               = vault_mount.kvv2.path
  name                = "voxmaster/test/develop/environment_variables"
  cas                 = 1
  delete_all_versions = true
  data_json = jsonencode({
    zip = "zap",
    foo = "foos"
  })
}
resource "vault_kv_secret_v2" "project_prod_secret" {
  mount               = vault_mount.kvv2.path
  name                = "voxmaster/test/prod/environment_variables"
  cas                 = 1
  delete_all_versions = true
  data_json = jsonencode({
    zip = "zap",
    foo = "foos"
  })
}


## Gitlab https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/
resource "vault_jwt_auth_backend" "gitlab_com" {
  description  = "Demonstration of the Terraform JWT auth backend"
  path         = "gitlab"
  jwks_url     = "https://gitlab.com/-/jwks"
  bound_issuer = "gitlab.com"
}

resource "vault_policy" "gitlab_read_develop" {
  name   = "gitlab-read-develop"
  ## NOTE: /data/ is required for v2 kv engine
  policy = <<EOT
path "${vault_mount.kvv2.path}/data/voxmaster/test/develop/*" {
  capabilities = ["read"]
}
EOT
}
resource "vault_policy" "gitlab_read_prod" {
  name   = "gitlab-read-prod"
  policy = <<EOT
path "${vault_mount.kvv2.path}/data/voxmaster/test/prod/*" {
  capabilities = ["read"]
}
EOT
}

resource "vault_jwt_auth_backend_role" "gitlab_read_develop" {
  backend        = vault_jwt_auth_backend.gitlab_com.path
  role_name      = "read-develop"
  token_policies = [vault_policy.gitlab_read_develop.name]

  # bound_audiences = ["https://myco.test"]
  bound_claims = {
    "project_id" = "23284015",
    "ref"        = "vault", # branch name(develop | prod)
    "ref_type"   = "branch"
  }
  user_claim = "user_email"
  role_type  = "jwt"
}

resource "vault_jwt_auth_backend_role" "gitlab_read_prod" {
  backend        = vault_jwt_auth_backend.gitlab_com.path
  role_name      = "read-prod"
  token_policies = [vault_policy.gitlab_read_prod.name]

  # bound_audiences = ["https://myco.test"]
  bound_claims = {
    "project_id" = "23284015",
    "ref"        = "prod", # branch name(develop | prod)
    "ref_type"   = "branch"
  }
  user_claim = "user_email"
  role_type  = "jwt"
}
