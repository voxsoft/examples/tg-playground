## Terraform Module
# Local

## Inlcudes
include "root" { # Root level config
  path = find_in_parent_folders("root.hcl")
}

generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider vault {
  address         = "https://vault.gcp.voxsoft.pro:8200"
  skip_tls_verify = true
  token           = "hvs.u3vQTOF6h9FeK7468NkqfEWj"
}
EOF
}
