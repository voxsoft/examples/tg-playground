module "vault" {
  # source     = "terraform-google-modules/vault/google"
  # version = "7.0.1"
  source     = "git::https://github.com/voxmaster/terraform-google-vault.git?ref=master" # fork

  project_id = var.project_id
  region     = var.region

  network                   = var.network
  subnet                    = var.subnet
  network_subnet_cidr_range = var.subnet == "" ? var.network_subnet_cidr_range : data.google_compute_subnetwork.provided[0].ip_cidr_range
  allow_public_egress       = var.allow_public_egress
  manage_tls                = var.manage_tls
  tls_cn                    = var.tls_cn

  storage_bucket_force_destroy = var.storage_bucket_force_destroy
  vault_version                = "1.13.2"
  vault_machine_type           = var.vault_machine_type
  vault_max_num_servers        = var.vault_max_num_servers
}

data "google_compute_subnetwork" "provided" {
  count   = var.subnet != "" ? 1 : 0
  name    = var.subnet
  region  = var.region
}


variable "project_id" {
  type        = string
  description = "ID of the project in which to create resources and add IAM bindings."
}
variable "region" {
  type        = string
  default     = "us-east4"
  description = "Region in which to create resources."
}
variable "storage_bucket_force_destroy" {
  type        = string
  default     = true
  description = "Set to true to force deletion of backend bucket on `terraform destroy`"
}
variable "network" {
  type        = string
  default     = ""
  description = "The self link of the VPC network for Vault. By default, one will be created for you."
}
variable "subnet" {
  type        = string
  default     = ""
  description = "The self link of the VPC subnetwork for Vault. By default, one will be created for you."
}
variable "allow_public_egress" {
  type        = bool
  default     = true
  description = "Whether to create a NAT for external egress. If false, you must also specify an `http_proxy` to download required executables including Vault, Fluentd and Stackdriver"
}
variable "manage_tls" {
  type        = bool
  default     = true
  description = "Set to `false` if you'd like to manage and upload your own TLS files. See `Managing TLS` for more details"
}
variable "tls_cn" {
  description = "The TLS Common Name for the TLS certificates"
  type        = string
  default     = "vault.example.net"
}
variable "vault_machine_type" {
  type        = string
  default     = "e2-small"
  description = "Machine type to use for Vault instances."
}
variable "vault_max_num_servers" {
  type        = string
  default     = "1"
  description = "Maximum number of Vault server nodes to run at one time. The group will not autoscale beyond this number."
}
variable "vault_tls_kms_key" {
  type        = string
  default     = ""
  description = "Fully qualified name of the KMS key, for example, vault_tls_kms_key = \"projects/PROJECT_ID/locations/LOCATION/keyRings/KEYRING/cryptoKeys/KEY_NAME\". This key should have been used to encrypt the TLS private key if Terraform is not managing TLS. The Vault service account will be granted access to the KMS Decrypter role once it is created so it can pull from this the `vault_tls_bucket` at boot time. This option is required when `manage_tls` is set to false."
}
variable "network_subnet_cidr_range" {
  type    = string
  default = "10.127.0.0/20"
  description = "CIDR block range for the subnet."
}
