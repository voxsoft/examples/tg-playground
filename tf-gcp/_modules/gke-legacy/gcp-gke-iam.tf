
#* IAM Service Account For Node Pools *#
resource "google_service_account" "sa_gke0_nodes" {
  project      = var.project_id
  account_id   = "sa-gke-${local.gke_cluster_name}-nodes"
  display_name = "sa-gke-${local.gke_cluster_name}-nodes"
  description  = "SA for GKE ${local.gke_cluster_name} Nodes. Managed by Terraform."
}
resource "google_project_iam_member" "sa_gke0_nodes" {
  for_each = toset(local.sa_gke0_nodes_roles)
  project  = var.project_id
  role     = each.key
  member   = "serviceAccount:${google_service_account.sa_gke0_nodes.email}"
}
locals {
  sa_gke0_nodes_roles = [
    "roles/artifactregistry.reader",
    "roles/cloudtrace.agent",
    "roles/logging.logWriter",
    "roles/monitoring.metricWriter",
    "roles/monitoring.viewer",
    "roles/stackdriver.resourceMetadata.writer",
  ]
}
