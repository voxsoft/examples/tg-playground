###* Networking Config *###
## VPC
resource "google_compute_network" "gke" {
  name                    = "${var.resource_name_prefix}-gke-${random_id.random_suffix.hex}"
  project                 = var.project_id
  auto_create_subnetworks = "false"
  routing_mode            = "GLOBAL"

  description = "GKE VPC. Managed by Terraform."
}

## Subnet for GKE
resource "google_compute_subnetwork" "gke" {
  name    = google_compute_network.gke.name
  project = var.project_id
  region  = var.region_default
  network = google_compute_network.gke.name

  private_ip_google_access = true

  ip_cidr_range = var.gcp_vpc_gke_node_range
  secondary_ip_range {
    range_name    = "${google_compute_network.gke.name}-pods"
    ip_cidr_range = var.gcp_vpc_gke_pod_range
  }
  secondary_ip_range {
    range_name    = "${google_compute_network.gke.name}-services"
    ip_cidr_range = var.gcp_vpc_gke_svc_range
  }
  description = "GKE Subnet. Managed by Terraform."
}

## Cloud NAT
# As we use GKE nodes without external IP address (= private cluster), we need to create a NAT router to give them access to Internet
resource "google_compute_router" "gke_nat" {
  name        = "${google_compute_network.gke.name}-router0"
  project     = var.project_id
  region      = var.region_default
  network     = google_compute_network.gke.name
  description = "Router in gke VPC. Managed by Terraform."
}
resource "google_compute_address" "gke_nat" {
  count        = 1
  provider     = google-beta
  project      = var.project_id
  region       = var.region_default
  name         = "${google_compute_router.gke_nat.name}-ip${count.index}"
  address_type = "EXTERNAL"
  network_tier = "PREMIUM"

  labels      = { managed_by = "terraform" }
  description = "NAT external IP address. Managed by Terraform."
}
resource "google_compute_router_nat" "gke_nat" {
  name                               = "${google_compute_router.gke_nat.name}-nat0"
  project                            = var.project_id
  region                             = var.region_default
  router                             = google_compute_router.gke_nat.name
  nat_ip_allocate_option             = "MANUAL_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
  nat_ips                            = google_compute_address.gke_nat[*].self_link
  log_config {
    enable = false
    filter = "ALL"
  }
}
output "gke_nat_ips" {
  value = google_compute_address.gke_nat[*].address
}

## Service Peering
resource "google_compute_global_address" "gke_private_ip_address" {
  count         = var.gcp_vpc_gke_servicenet_create ? 1 : 0
  provider      = google-beta
  name          = "${google_compute_network.gke.name}-svcnet-ip${count.index}"
  project       = var.project_id
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  address       = split("/", var.gcp_vpc_gke_servicenet_range).0
  prefix_length = split("/", var.gcp_vpc_gke_servicenet_range).1
  network       = google_compute_network.gke.self_link

  labels      = { managed_by = "terraform" }
  description = "Service peering IP address. Managed by Terraform."
}
resource "google_service_networking_connection" "gke_private_vpc_connection" {
  count                   = var.gcp_vpc_gke_servicenet_create ? 1 : 0
  provider                = google-beta
  network                 = google_compute_network.gke.self_link
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.gke_private_ip_address[0].name]
}
/*
## Serverless VPC connector
resource "google_vpc_access_connector" "gke" {
  name          = "${google_compute_network.gke.name}-conn"
  project       = var.project_id
  region        = var.region_default
  network       = google_compute_network.gke.name
  ip_cidr_range = var.gcp_vpc_gke_connector_range
  machine_type = "f1-micro"
}
*/

###* Firewall *###
## IAP TCP forwarding
resource "google_compute_firewall" "gke_iap" {
  name    = "${google_compute_network.gke.name}-allow-ingress-iap"
  project = var.project_id
  network = google_compute_network.gke.name
  allow {
    protocol = "tcp"
    ports    = ["22", "3389"]
  }
  source_ranges = ["35.235.240.0/20"]
  # target_tags = ["ssh", "rdp"]
  description = "Allow connection from Identity-Aware Proxy. Managed by Terraform."
}

###* GKE External Ingress IP *###
resource "google_compute_address" "gke_ingress" {
  count        = 1
  provider     = google-beta
  project      = var.project_id
  region       = var.region_default
  name         = "${google_compute_network.gke.name}-ing-ip${count.index}"
  address_type = "EXTERNAL"
  network_tier = "PREMIUM"

  labels      = { managed_by = "terraform" }
  description = "Ingress Static IP address for GKE Cluster. Managed by Terraform."
}
output "gke_ingress_ips" {
  value = var.gke_cluster_create ? google_compute_address.gke_ingress[*].address : []
}
output "network_name" {
  value = google_compute_network.gke.name
}
output "network_self_link" {
  value = google_compute_network.gke.self_link
}
