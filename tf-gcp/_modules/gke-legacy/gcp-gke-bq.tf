## BigQuery (SS SecOps)
# TODO: Add BQ dataset for GKE egress metering as an option
resource "google_bigquery_dataset" "gke" {
  count         = 0
  project       = var.project_id
  dataset_id    = "gke"
  friendly_name = "gke"
  description   = "GKE traffic egress metering"
  location      = "US"
  # default_table_expiration_ms = 3600000
  labels = { managed_by = "terraform" }
}
