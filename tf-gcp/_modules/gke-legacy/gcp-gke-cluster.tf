###* GKE-0 Cluster Related Configuration *###
locals {
  gke_authorized_networks = length(var.gke_authorized_networks) == 0 ? [] : [{
    cidr_blocks : var.gke_authorized_networks
  }]
}

#* GKE Cluster *#
resource "google_container_cluster" "cluster0" {
  count      = var.gke_cluster_create ? 1 : 0
  depends_on = [google_compute_network.gke]
  provider   = google-beta
  project    = var.project_id
  name       = local.gke_cluster_name
  location   = var.gke_cluster_regional ? var.region_default : var.zone_default
  release_channel {
    channel = "STABLE"
  }
  maintenance_policy {
    recurring_window {
      start_time = "2020-01-19T04:00:00Z"
      end_time   = "2020-01-19T09:00:00Z"
      recurrence = "FREQ=WEEKLY;BYDAY=SU,FR,SA"
    }
  }
  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1
  master_auth {
    client_certificate_config {
      issue_client_certificate = false
    }
  }
  dynamic "master_authorized_networks_config" {
    for_each = local.gke_authorized_networks
    content {
      dynamic "cidr_blocks" {
        for_each = master_authorized_networks_config.value.cidr_blocks
        content {
          cidr_block   = lookup(cidr_blocks.value, "cidr_block", "")
          display_name = lookup(cidr_blocks.value, "display_name", "")
        }
      }
    }
  }
  # `logging_service` and `monitoring_service` must have identical values if disabled (set to "none")
  # WARNING: Enabling logging and monitoring will increase project costs significantly!
  logging_service    = var.gke_native_logging ? "logging.googleapis.com/kubernetes" : "none"
  monitoring_service = var.gke_native_logging ? "monitoring.googleapis.com/kubernetes" : "none"
  # ip_allocation_policy enables VPC-native cluster instead of routes-based.
  ip_allocation_policy {
    cluster_secondary_range_name  = google_compute_subnetwork.gke.secondary_ip_range.0.range_name
    services_secondary_range_name = google_compute_subnetwork.gke.secondary_ip_range.1.range_name
  }
  network                   = google_compute_network.gke.self_link
  subnetwork                = google_compute_subnetwork.gke.self_link
  default_max_pods_per_node = "110" # https://cloud.google.com/kubernetes-engine/docs/how-to/flexible-pod-cidr
  private_cluster_config {
    enable_private_endpoint = false
    enable_private_nodes    = true
    master_ipv4_cidr_block  = var.gcp_vpc_gke_master_range
  }
  # vertical_pod_autoscaling { # * Not sure if this is required
  #   enabled = true
  # }
  # database_encryption {
  #   key_name = google_kms_crypto_key.infrastructure_default_gke.id
  #   state    = "ENCRYPTED"
  # }
  # pod_security_policy_config {
  #   enabled = var.gcp_gke_pod_security_policy
  # }
  dynamic "network_policy" {
    for_each = local.cluster_network_policy

    content {
      enabled  = network_policy.value.enabled
      provider = network_policy.value.provider
    }
  }
  # resource_usage_export_config {
  #   enable_network_egress_metering       = true 
  #   enable_resource_consumption_metering = true
  #   bigquery_destination {
  #     dataset_id = google_bigquery_dataset.gke[0].dataset_id
  #   }
  # }
  addons_config {
    http_load_balancing {
      disabled = false
    }
    network_policy_config {
      disabled = var.gke_network_policy_provider == null ? true : false
    }
  }
  node_config {
    service_account = google_service_account.sa_gke0_nodes.email
  }
  workload_identity_config {
    workload_pool = "${var.project_id}.svc.id.goog"
  }

  resource_labels = { managed_by = "terraform" }

  lifecycle {
    ignore_changes = [
      node_config # We are using remove_default_node_pool = true, so we should ignore changes to node_config
    ]
  }
}

#!

#* GKE Node Pools *#
## TODO: Move to module, use lookup() for default values
# GKE Node Pool # | #~ Default | KEEPERS ~#
resource "random_id" "cluster0_np_default" {
  keepers = {
    "enabled"      = var.gcp_gke_pool_default_config.enable
    "machine_type" = var.gcp_gke_pool_default_config.machine_type
    "preemptible"  = var.gcp_gke_pool_default_config.preemptible
    "disk_type"    = var.gcp_gke_pool_default_config.disk_type
    "disk_size_gb" = var.gcp_gke_pool_default_config.disk_size_gb
  }
  byte_length = "2"
}
# GKE Node Pool # | #~ Default ~#
resource "google_container_node_pool" "cluster0_np_default" {
  count      = var.gcp_gke_pool_default_config.enable && var.gke_cluster_create ? 1 : 0
  depends_on = [google_container_cluster.cluster0]
  name       = "default-${random_id.cluster0_np_default.hex}"
  project    = var.project_id
  location   = google_container_cluster.cluster0[0].location
  cluster    = google_container_cluster.cluster0[0].name

  node_count = var.gcp_gke_pool_default_config.min_node_count
  autoscaling {
    min_node_count = var.gcp_gke_pool_default_config.min_node_count
    max_node_count = var.gcp_gke_pool_default_config.max_node_count
  }
  management {
    auto_repair  = true
    auto_upgrade = true
  }
  node_config {
    preemptible     = var.gcp_gke_pool_default_config.preemptible
    machine_type    = var.gcp_gke_pool_default_config.machine_type
    disk_type       = var.gcp_gke_pool_default_config.disk_type
    disk_size_gb    = var.gcp_gke_pool_default_config.disk_size_gb
    service_account = google_service_account.sa_gke0_nodes.email
    oauth_scopes    = ["https://www.googleapis.com/auth/cloud-platform"]
    tags            = [ "gke-${google_container_cluster.cluster0[0].name}" ]
    labels = {
      workload-type = "application"
      workload-env  = var.env_name
      preemptible   = var.gcp_gke_pool_default_config.preemptible
    }
    metadata = {
      disable-legacy-endpoints = "true"
      block-project-ssh-keys   = "true"
    }
  }

  # Because autoscaling is enabled we have no control over node_count change
  lifecycle {
    ignore_changes        = [node_count, initial_node_count]
    create_before_destroy = true
  }
}



#* GKE Firewall Webhooks *#
#* Firewall rule for GKE cluster to be able to use Kubernetes Operators (Master webhooks)
# doc1: https://kubernetes.github.io/ingress-nginx/deploy/#gce-gke
# doc2: https://cloud.google.com/kubernetes-engine/docs/how-to/private-clusters#add_firewall_rules
resource "google_compute_firewall" "gke_cluster0_master_webhooks" {
  count      = var.gke_cluster_create ? 1 : 0
  depends_on = [google_container_cluster.cluster0]
  name       = "gke-${substr(google_container_cluster.cluster0[0].name, 0, min(25, length(google_container_cluster.cluster0[0].name)))}-webhooks"
  project    = var.project_id
  network    = google_compute_network.gke.name
  allow {
    protocol = "tcp"
    ports    = ["8443"]
  }
  source_ranges = [var.gcp_vpc_gke_master_range]
  target_tags = [
    "gke-${google_container_cluster.cluster0[0].name}",
  ]
  description = "Firewall rule for ${google_container_cluster.cluster0[0].name} GKE cluster to be able to send webhooks from master. Managed by Terraform."
}


output "gke_cluster_name" {
  value = google_container_cluster.cluster0[0].name
}
