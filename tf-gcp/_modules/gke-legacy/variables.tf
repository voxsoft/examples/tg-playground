
# Global Settings
variable "env_name" {
  description = "Environment name"
  default     = "primer"
}
variable "project_id" {
  description = "GCP project id"
}
variable "resource_name_prefix" {
  default     = null
  description = "GCP project readable id. Unique across GCP projects. Written without spaces and dots. Will be used in cluster names, bucket names, etc. If not set, will be generated from project_id"
}
variable "region_default" {
  default     = "us-central1"
  description = "GCP default region"
}
variable "zone_default" {
  default     = "us-central1-c"
  description = "GCP default zone. Must correspond with `region_default` variable"
}

# VPC and VPN Settings (plus GKE IP allocation)
variable "gcp_project_ip_range" {
  default     = "10.132.0.0/15"
  description = "GCP VPC ip range. Informative + This value will be used in GCP-AWS VPN as main route"
}
variable "gcp_vpc_gke_pod_range" {
  default     = "10.132.0.0/18"
  description = "GKE cluster Pod IP range"
}
variable "gcp_vpc_gke_svc_range" {
  default     = "10.132.64.0/20"
  description = "GKE cluster Service IP range"
}
variable "gcp_vpc_gke_node_range" {
  default     = "10.132.80.0/20"
  description = "GCP default subnet IP range"
}
variable "gcp_vpc_gke_servicenet_create" {
  default     = false
  description = "Service networking create"
}
variable "gcp_vpc_gke_servicenet_range" {
  default     = "10.132.96.0/20"
  description = "Service networking range: https://cloud.google.com/vpc/docs/configure-private-services-access"
}
variable "gcp_vpc_gke_master_range" {
  default     = "10.132.127.0/28"
  description = "GKE cluster Master IP range. /28 is typical"
}
variable "gcp_vpc_gke_connector_range" {
  default     = "10.132.127.32/28"
  description = "VPC Serverless connector IP range. /28 is typical"
}

# GKE Settings
variable "gke_cluster_create" {
  default     = true
  description = "Create GKE cluster"
}
variable "gke_cluster_name" {
  default     = null
  description = "Name of the GKE cluster. If not specified, will be generated from `resource_name_prefix`"
}
variable "gke_cluster_regional" {
  default     = false
  description = "Setup regional GKE Cluster if `true`, otherwise - zonal"
}
variable "gke_network_policy_provider" {
  type        = string
  description = "The network policy provider typically `CALICO`. If not specified - network_policy will be disabled"
  default     = null
}
variable "gke_authorized_networks" {
  default = [
    { cidr_block = "0.0.0.0/0", display_name = "Open to internet" },
  ]
  type        = list(object({ cidr_block = string, display_name = string }))
  description = "List of master authorized networks. If none are provided, disallow external access (except the cluster node IPs, which GKE automatically whitelists)."
}
variable "node_pools" {
  type        = list(map(any))
  description = "List of maps containing node pools configurations."
  default = [
    {
      name           = "default"
      enable         = true
      machine_type   = "e2-medium"
      min_node_count = 1
      max_node_count = 2
      disk_type      = "pd-standard"
      disk_size_gb   = 35
      preemptible    = true
    },
  ]
}

### LEGACY POOL CONFIGURATION
variable "gcp_gke_pool_default_config" {
  ## TODO: object(map(...)) / Use version above
  type = map(any)
  default = {
    enable         = true
    machine_type   = "e2-medium"
    min_node_count = 1
    max_node_count = 2
    disk_type      = "pd-standard"
    disk_size_gb   = 35
    preemptible    = true
  }
  description = "Default nodepool config of the GKE cluster"
}
# Logging / Audit
variable "gke_native_logging" {
  default     = false
  description = "WARNING: Enabling `logging_service` and `monitoring_service` will increase project costs significantly!"
}
