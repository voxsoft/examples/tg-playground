###* Random ID *###
resource "random_id" "random_suffix" {
  byte_length = 2
}

locals {
  resource_name_prefix = var.resource_name_prefix == null ? var.project_id : var.resource_name_prefix
  gke_cluster_name     = var.gke_cluster_name == null ? "${var.resource_name_prefix}-${random_id.random_suffix.hex}" : var.gke_cluster_name
  cluster_network_policy = var.gke_network_policy_provider != null ? [{
    enabled  = true
    provider = var.gke_network_policy_provider
    }] : [{
    enabled  = false
    provider = null
  }]
}

###* Common data resources *###
# data "google_project" "project" {}
# data "google_compute_default_service_account" "default" {}
# data "google_storage_project_service_account" "default" {}
