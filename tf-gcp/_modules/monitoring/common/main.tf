## NOTE: "Notify on incident closure" can not be controlled via TF/API:
##   https://github.com/hashicorp/terraform-provider-google/issues/8470 
##   so this is unfortunately a manual step for now, to reduce email noise.
locals {
  ## Merge all emails into a single list
  notification_emails_all = distinct(flatten(values(var.ops_notification_emails)))
  ## Get the IDs of the email notification channels
  notification_emails_channel_ids = {
    for group, emails in var.ops_notification_emails : group => [for email in emails :
      google_monitoring_notification_channel.emails[email].id
    ]
  }
  ## This local matches the alert_policy name to the list of notification channels
  ## Change it to add to or remove notification channels from a given alert policy
  alert_policy_notification_channels = {
    iam_sa_key_created = distinct(concat(
      local.notification_emails_channel_ids.ops,
      local.notification_emails_channel_ids.devs,
      local.notification_emails_channel_ids.managers,
    ))
  }
}
### Notification Channels ###
resource "google_monitoring_notification_channel" "emails" {
  for_each     = toset(local.notification_emails_all)
  display_name = each.key
  type         = "email"
  labels       = { email_address = each.key }
  user_labels  = { managed_by = "terraform" }
}


## Log based alerting
resource "google_monitoring_alert_policy" "iam_sa_key_created" {
  display_name          = "IAM | Service Account Key Created (Unauthorized)"
  combiner              = "OR"
  notification_channels = local.alert_policy_notification_channels.iam_sa_key_created
  conditions {
    display_name = "${upper(var.env_name)} | IAM | Service Account Key Created (Unauthorized)"
    condition_matched_log {
      filter = <<-EOT
        logName="projects/${var.project_id}/logs/cloudaudit.googleapis.com%2Factivity"
        resource.type="service_account"
        protoPayload.methodName="google.iam.admin.v1.CreateServiceAccountKey"
        protoPayload.authenticationInfo.principalEmail !~ "^someuser@gmail.com$"
      EOT
      label_extractors = {
        "keyCreator" = "EXTRACT(protoPayload.authenticationInfo.principalEmail)"
      }
    }
  }
  alert_strategy {
    auto_close = "604800s"
    notification_rate_limit {
      period = "300s"
    }
  }
  user_labels = { managed_by = "terraform" }
  documentation {
    content = <<-EOT
      Service Account Key Created manually outside of Terraform! This might be unauthorized action! 
      Please review if an authorized person created the Service Account Key and if there is a necessity to keep the created Key.
      Revoke the SA Key if necessary.
    EOT
  }
  lifecycle { ignore_changes = [enabled] }
}
