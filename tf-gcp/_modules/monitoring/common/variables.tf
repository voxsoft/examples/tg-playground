variable "project_id" {
  description = "The project ID to deploy to"
}
variable "env_name" {
  description = "The environment to deploy to"
}

variable "ops_notification_emails" {
  type = object({
    ops      = list(string)
    devs     = list(string)
    managers = list(string)
  })
  description = "Cloud Operations: Emails to send alerts to, split by groups (ops, devs, managers), to address the right people"
  default = {
    ops      = []
    devs     = []
    managers = []
  }
}
