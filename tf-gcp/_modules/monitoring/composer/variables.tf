variable "project_id" {
  description = "The project ID to deploy to"
}
variable "env_name" {
  description = "The environment to deploy to"
}
variable "logging_metrics_create" {
  default     = false
  type        = bool
  description = "Whether to create logging metrics or not. this will affect the creation of alerts depended on logging metrics"
}
variable "composer_gke_name" {
  description = "The name of the GKE cluster that Composer is running on"
  type        = string
}

variable "ops_notification_emails" {
  type = object({
    ops      = list(string)
    devs     = list(string)
    managers = list(string)
  })
  description = "Cloud Operations: Emails to send alerts to, split by groups (ops, devs, managers), to address the right people"
  default = {
    ops      = []
    devs     = []
    managers = []
  }
}
