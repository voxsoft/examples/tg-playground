locals {
  logging_metrics_create = false
}

## Log-based Metric | Composer Workflow Error - SIGTERM
resource "google_logging_metric" "composer_workflow_err_sigterm" {
  count  = local.logging_metrics_create ? 1 : 0
  name   = "composer/workflow/err_sigterm"
  filter = <<-EOT
    resource.type="cloud_composer_environment"
    labels.workflow:*
    severity=ERROR
    textPayload=~ "Received SIGTERM"
  EOT
  metric_descriptor {
    metric_kind = "DELTA"
    value_type  = "INT64"
    labels {
      key        = "workflow"
      value_type = "STRING"
    }
    labels {
      key        = "taskid"
      value_type = "STRING"
    }
  }
  label_extractors = {
    "workflow" = "EXTRACT(labels.workflow)"
    "taskid"   = "EXTRACT(labels.\"task-id\")"
  }
}

## Log-based Metric | Composer Workflow - SIGKILL
resource "google_logging_metric" "composer_workflow_sigkill" {
  count  = local.logging_metrics_create ? 1 : 0
  name   = "composer/workflow/sigkill"
  filter = <<-EOT
    resource.type="cloud_composer_environment"
    labels.workflow:*
    textPayload=~ "SIGKILL"
  EOT
  metric_descriptor {
    metric_kind = "DELTA"
    value_type  = "INT64"
    labels {
      key        = "workflow"
      value_type = "STRING"
    }
    labels {
      key        = "taskid"
      value_type = "STRING"
    }
  }
  label_extractors = {
    "workflow" = "EXTRACT(labels.workflow)"
    "taskid"   = "EXTRACT(labels.\"task-id\")"
  }
}
