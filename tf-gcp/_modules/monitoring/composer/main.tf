## NOTE: "Notify on incident closure" can not be controlled via TF/API:
##   https://github.com/hashicorp/terraform-provider-google/issues/8470 
##   so this is unfortunately a manual step for now, to reduce email noise.
locals {
  ## Merge all emails into a single list
  notification_emails_all = distinct(flatten(values(var.ops_notification_emails)))
  ## Get the IDs of the email notification channels
  notification_emails_channel_ids = {
    for group, emails in var.ops_notification_emails : group => [for email in emails :
      google_monitoring_notification_channel.emails[email].id
    ]
  }
  ## This local matches the alert_policy name to the list of notification channels
  ## Change it to add to or remove notification channels from a given alert policy
  alert_policy_notification_channels = {
    vm_resource_utilization = distinct(concat(
      local.notification_emails_channel_ids.ops,
    ))
    composer_resource_utilization = distinct(concat(
      local.notification_emails_channel_ids.ops,
    ))
    composer_db_resource_utilization = distinct(concat(
      local.notification_emails_channel_ids.ops,
    ))
    composer_workflow_duration = distinct(concat(
      local.notification_emails_channel_ids.ops,
      local.notification_emails_channel_ids.devs,
    ))
    composer_workflow_failures = distinct(concat(
      local.notification_emails_channel_ids.ops,
      local.notification_emails_channel_ids.devs,
    ))
    composer_workflow_err_sigterm = distinct(concat(
      local.notification_emails_channel_ids.ops,
      local.notification_emails_channel_ids.devs,
    ))
    composer_workflow_oomkill = distinct(concat(
      local.notification_emails_channel_ids.ops,
      local.notification_emails_channel_ids.devs,
    ))
    iam_sa_key_created = distinct(concat(
      local.notification_emails_channel_ids.ops,
      local.notification_emails_channel_ids.devs,
      local.notification_emails_channel_ids.managers,
    ))
  }
}

### Notification Channels ###
resource "google_monitoring_notification_channel" "emails" {
  for_each     = toset(local.notification_emails_all)
  display_name = each.key
  type         = "email"
  labels       = { email_address = each.key }
  user_labels  = { managed_by = "terraform" }
}



### VM | Alert Policies ###
resource "google_monitoring_alert_policy" "vm_resource_utilization" {
  display_name          = "VM | Resource Utilization"
  combiner              = "OR"
  notification_channels = local.alert_policy_notification_channels.vm_resource_utilization

  ## CPU
  conditions {
    display_name = "${upper(var.env_name)} | VM | CPU Utilization > 90% for 600s"
    condition_threshold {
      filter = <<-EOT
        resource.type = "gce_instance"
        metric.type = "agent.googleapis.com/cpu/utilization"
        metric.labels.cpu_state = "user"
        metadata.system_labels.name != has_substring("tes")
      EOT
      aggregations {
        alignment_period   = "600s"
        per_series_aligner = "ALIGN_MEAN"
      }
      comparison      = "COMPARISON_GT"
      threshold_value = 90
      duration        = "600s"
      trigger { count = 1 }
    }
  }

  ## MEM
  conditions {
    display_name = "${upper(var.env_name)} | VM | Memory Utilization > 90% for 600s"
    condition_threshold {
      filter = <<-EOT
        resource.type = "gce_instance"
        metric.type = "agent.googleapis.com/memory/percent_used"
        metric.labels.state = "used"
        metadata.system_labels.name != has_substring("tes")
      EOT
      aggregations {
        per_series_aligner = "ALIGN_MEAN"
        alignment_period   = "600s"
      }
      comparison      = "COMPARISON_GT"
      threshold_value = 90
      duration        = "600s"
      trigger { count = 1 }
    }
  }

  ## DISK
  conditions {
    display_name = "${upper(var.env_name)} | VM | Disk Utilization > 80% for 600s"
    condition_threshold {
      filter = <<-EOT
        resource.type = "gce_instance"
        metric.type = "agent.googleapis.com/disk/percent_used"
        metric.labels.state = "used"
        metadata.system_labels.name != has_substring("tes")
      EOT
      aggregations {
        per_series_aligner = "ALIGN_MAX"
        alignment_period   = "600s"
      }
      comparison      = "COMPARISON_GT"
      threshold_value = 80
      duration        = "600s"
      trigger { count = 1 }
    }
  }

  alert_strategy { auto_close = "86400s" }
  user_labels = { managed_by = "terraform" }
  documentation {
    content = <<-EOT
      VM resource utilization is high. Check the specific VM metrics for more details.
    EOT
  }
  lifecycle { ignore_changes = [enabled] }
}



### Composer | Alert Policies ###
resource "google_monitoring_alert_policy" "composer_resource_utilization" {
  display_name          = "Composer | Resource Utilization"
  combiner              = "OR"
  notification_channels = local.alert_policy_notification_channels.composer_resource_utilization

  ## CPU
  conditions {
    display_name = "${upper(var.env_name)} | Composer | Resource CPU Limit Usage > 90% for 600s"
    condition_threshold {
      filter = <<-EOT
        resource.type = "k8s_container"
        metric.type = "kubernetes.io/container/cpu/limit_utilization"
        resource.labels.cluster_name = "${var.composer_gke_name}"
        resource.labels.pod_name = monitoring.regex.full_match("airflow-worker-.*|airflow-scheduler-.*|airflow-webserver-.*")
        resource.labels.pod_name != monitoring.regex.full_match("airflow-worker-set-.*")
        resource.labels.container_name != "gcs-syncd"
      EOT
      aggregations {
        cross_series_reducer = "REDUCE_MEAN"
        group_by_fields      = ["resource.label.pod_name"]
        per_series_aligner   = "ALIGN_MEAN"
        alignment_period     = "300s"
      }
      comparison      = "COMPARISON_GT"
      threshold_value = 0.90
      duration        = "300s"
      trigger { count = 2 }
    }
  }

  ## MEM
  conditions {
    display_name = "${upper(var.env_name)} | Composer | Resource Memory Limit Usage > 85% for 600s"
    condition_threshold {
      filter = <<-EOT
        resource.type = "k8s_container"
        metric.type = "kubernetes.io/container/memory/limit_utilization"
        resource.labels.cluster_name = "${var.composer_gke_name}"
        resource.labels.pod_name = monitoring.regex.full_match("airflow-worker-.*|airflow-scheduler-.*|airflow-webserver-.*")
        resource.labels.pod_name != monitoring.regex.full_match("airflow-worker-set-.*")
        resource.labels.container_name != "gcs-syncd"
      EOT
      aggregations {
        cross_series_reducer = "REDUCE_MAX"
        group_by_fields      = ["resource.label.pod_name"]
        per_series_aligner   = "ALIGN_MEAN"
        alignment_period     = "60s"
      }
      comparison      = "COMPARISON_GT"
      threshold_value = 0.85
      duration        = "600s"
      trigger { count = 1 }
    }
  }

  alert_strategy { auto_close = "86400s" }
  user_labels = { managed_by = "terraform" }
  documentation {
    content = <<-EOT
      Some of the Composer resource limit was (almost) reached.
      Check the resources utilization at the alert time. Consider to: 
      increase the Composer resource limits / Shift the DAG execution time to off-peak hours / Optimize the DAG's resource usage.  
      Refer to https://cloud.google.com/composer/docs/composer-2/optimize-environments for more details.
    EOT
  }
  lifecycle { ignore_changes = [enabled] }
}

resource "google_monitoring_alert_policy" "composer_db_resource_utilization" {
  display_name          = "Composer | Metadata DB | Resource Utilization"
  combiner              = "OR"
  notification_channels = local.alert_policy_notification_channels.composer_db_resource_utilization

  ## SQL CPU
  conditions {
    display_name = "${upper(var.env_name)} | Composer | Metadata DB | CPU Utilization > 90% for 600s"
    condition_threshold {
      filter = <<-EOT
        resource.type = "cloud_composer_environment"
        resource.labels.environment_name = "composer${substr(var.env_name, 0, 1)}0"
        metric.type = "composer.googleapis.com/environment/database/cpu/utilization"
      EOT
      aggregations {
        cross_series_reducer = "REDUCE_MEAN"
        group_by_fields      = ["resource.label.pod_name"]
        per_series_aligner   = "ALIGN_MAX"
        alignment_period     = "300s"
      }
      comparison      = "COMPARISON_GT"
      threshold_value = 0.95
      duration        = "300s"
      trigger { count = 2 }
    }
  }

  ## SQL MEM
  conditions {
    display_name = "${upper(var.env_name)} | Composer | Metadata DB | Memory Utilization > 85% for 600s"
    condition_threshold {
      filter = <<-EOT
        resource.type = "cloud_composer_environment"
        resource.labels.environment_name = "composer${substr(var.env_name, 0, 1)}0"
        metric.type = "composer.googleapis.com/environment/database/memory/utilization"

      EOT
      aggregations {
        cross_series_reducer = "REDUCE_MAX"
        group_by_fields      = ["resource.label.pod_name"]
        per_series_aligner   = "ALIGN_MEAN"
        alignment_period     = "60s"
      }
      comparison      = "COMPARISON_GT"
      threshold_value = 0.85
      duration        = "600s"
      trigger { count = 1 }
    }
  }

  ## SQL DISK
  conditions {
    display_name = "${upper(var.env_name)} | Composer | Metadata DB | DB Size > 15 GB"
    condition_threshold {
      filter = <<-EOT
        resource.type = "cloud_composer_environment"
        resource.labels.environment_name = "composer${substr(var.env_name, 0, 1)}0"
        metric.type = "composer.googleapis.com/environment/database/disk/bytes_used"
      EOT
      aggregations {
        per_series_aligner = "ALIGN_MAX"
        alignment_period   = "600s"
      }
      comparison      = "COMPARISON_GT"
      threshold_value = "16106127360" # 15GB
      duration        = "600s"
      trigger { count = 1 }
    }
  }

  alert_strategy { auto_close = "86400s" }
  user_labels = { managed_by = "terraform" }
  documentation {
    content = <<-EOT
      Some of the Composer SQL metadata DB resource limit was (almost) reached.
      Check the resources utilization at the alert time. Consider to: 
      increase the Composer Environment size or shift the DAG execution time to off-peak hours.
      If alert related to DB Size metric, consider to [Clean up the Airflow database](https://cloud.google.com/composer/docs/composer-2/cleanup-airflow-database)
      Refer to https://cloud.google.com/composer/docs/composer-2/optimize-environments for more details.
    EOT
  }
  lifecycle { ignore_changes = [enabled] }
}

resource "google_monitoring_alert_policy" "composer_workflow_duration" {
  display_name          = "Composer | Workflow Run Duration"
  combiner              = "OR"
  notification_channels = local.alert_policy_notification_channels.composer_workflow_duration
  conditions {
    display_name = "${upper(var.env_name)} | Composer | Workflow Run Duration > 3h"
    condition_threshold {
      filter = <<-EOT
        resource.type = "cloud_composer_workflow"
        metric.type = "composer.googleapis.com/workflow/dag/run_duration"
      EOT
      aggregations {
        per_series_aligner = "ALIGN_MAX"
        alignment_period   = "600s"
      }
      comparison      = "COMPARISON_GT"
      threshold_value = "10800000" # 3h in ms
      duration        = "600s"
      trigger { count = 1 }
    }
  }
  alert_strategy { auto_close = "3600s" }
  user_labels = { managed_by = "terraform" }
  documentation {
    content = <<-EOT
      Composer Workflow has been running for more than 3 hours. If possible, try to optimize the DAG execution time.
    EOT
  }
  lifecycle { ignore_changes = [enabled] }
}

resource "google_monitoring_alert_policy" "composer_workflow_failures" {
  display_name          = "Composer | Workflow Run Failure"
  combiner              = "OR"
  notification_channels = local.alert_policy_notification_channels.composer_workflow_failures
  conditions {
    display_name = "${upper(var.env_name)} | Composer | Workflow Run Failure"
    condition_threshold {
      filter = <<-EOT
        resource.type = "cloud_composer_workflow"
        metric.type = "composer.googleapis.com/workflow/run_count"
        metric.labels.state = "failed"
      EOT
      aggregations {
        cross_series_reducer = "REDUCE_COUNT"
        group_by_fields      = ["resource.label.workflow_name"]
        per_series_aligner   = "ALIGN_COUNT"
        alignment_period     = "60s"
      }
      comparison      = "COMPARISON_GT"
      threshold_value = "0"
      duration        = "300s"
      trigger { count = 1 }
    }
  }
  alert_strategy { auto_close = "86400s" }
  user_labels = { managed_by = "terraform" }
  documentation {
    content = <<-EOT
      Composer Workflow Run fails. Check the specific Workflow Run logs for more details.
    EOT
  }
  lifecycle { ignore_changes = [enabled] }
}

resource "google_monitoring_alert_policy" "composer_workflow_err_sigterm" {
  count                 = var.logging_metrics_create ? 1 : 0
  display_name          = "Composer | Workflow Received SIGTERM"
  combiner              = "OR"
  notification_channels = local.alert_policy_notification_channels.composer_workflow_err_sigterm
  conditions {
    display_name = "${upper(var.env_name)} | Composer | Workflow Received SIGTERM"
    condition_threshold {
      filter = <<-EOT
        resource.type = "cloud_composer_environment"
        metric.type = "logging.googleapis.com/user/${google_logging_metric.composer_workflow_err_sigterm[0].id}"
      EOT
      aggregations {
        cross_series_reducer = "REDUCE_COUNT"
        group_by_fields      = ["metric.label.workflow"]
        per_series_aligner   = "ALIGN_COUNT"
        alignment_period     = "300s"
      }
      comparison      = "COMPARISON_GT"
      threshold_value = "0"
      duration        = "300s"
      trigger { count = 1 }
    }
  }
  alert_strategy { auto_close = "86400s" }
  user_labels = { managed_by = "terraform" }
  documentation {
    content = <<-EOT
      Composer Workflow Run received SIGTERM. This might be due to a timeout or manual termination.
    EOT
  }
  lifecycle { ignore_changes = [enabled] }
}

resource "google_monitoring_alert_policy" "composer_workflow_oomkill" {
  count                 = var.logging_metrics_create ? 1 : 0
  display_name          = "Composer | Workflow Task OOM Killed"
  combiner              = "AND"
  notification_channels = local.alert_policy_notification_channels.composer_workflow_oomkill
  conditions {
    display_name = "${upper(var.env_name)} | Composer | Workflow Received SIGKILL"
    condition_threshold {
      filter = <<-EOT
        resource.type = "cloud_composer_environment"
        metric.type = "logging.googleapis.com/user/${google_logging_metric.composer_workflow_sigkill[0].id}"
      EOT
      aggregations {
        cross_series_reducer = "REDUCE_COUNT"
        group_by_fields      = ["metric.label.workflow", "metric.label.taskid"]
        per_series_aligner   = "ALIGN_COUNT"
        alignment_period     = "60s"
      }
      comparison      = "COMPARISON_GT"
      threshold_value = "0"
      duration        = "60s"
      trigger { count = 1 }
      evaluation_missing_data = "EVALUATION_MISSING_DATA_NO_OP"
    }
  }
  conditions {
    display_name = "${upper(var.env_name)} | Composer | Worker Memory Limit Usage > 95%"
    condition_threshold {
      filter = <<-EOT
        resource.type = "k8s_container"
        metric.type = "kubernetes.io/container/memory/limit_utilization"
        resource.labels.cluster_name = "${var.composer_gke_name}"
        resource.labels.pod_name = monitoring.regex.full_match("airflow-worker-.*")
        resource.labels.pod_name != monitoring.regex.full_match("airflow-worker-set-.*")
      EOT
      aggregations {
        cross_series_reducer = "REDUCE_SUM"
        group_by_fields      = ["resource.label.pod_name"]
        per_series_aligner   = "ALIGN_MEAN"
        alignment_period     = "60s"
      }
      comparison      = "COMPARISON_GT"
      threshold_value = 0.95
      duration        = "60s"
      trigger { count = 1 }
    }
  }
  alert_strategy { auto_close = "86400s" }
  user_labels = { managed_by = "terraform" }
  documentation {
    content = <<-EOT
      Composer Workflow Run Task received SIGKILL. This might be due to a OOM, because the memory limit of worker was (almost) reached.
      Check the Memory utilization at the alert time. Consider to: 
      increase the Composer Workers memory / Shift the DAG execution time to off-peak hours / Optimize the DAG memory usage.  
      Refer to https://cloud.google.com/composer/docs/composer-2/optimize-environments for more details.
    EOT
  }
  lifecycle { ignore_changes = [enabled] }
}

