module "tls" {
  count = var.ssl_certificate_generate_self_signed ? 1 : 0
  source = "./examples/gcp-tls"

  project_id = var.project_id
  region     = var.region
  shared_san = var.leader_tls_servername
  # tls_secret_id = 
}

output "ssl_certificate_name" {
  value       = var.ssl_certificate_generate_self_signed ? module.tls[0].ssl_certificate_name : var.ssl_certificate_name
  description = "Name of the ssl certificate resource"
}
