include "root" {
  path = find_in_parent_folders()
}
dependencies {
  paths = ["../vpc"]
}
dependency "vpc" {
  config_path = "../vpc"
}
terraform {
  source = "${get_parent_terragrunt_dir()}/_modules//vault-starter"
}

generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "google" {
  project     = "${local.project_vars.locals.gcp_project_id}"
  region      = "${local.project_vars.locals.gcp_region_default}"
}
provider "google-beta" {
  project     = "${local.project_vars.locals.gcp_project_id}"
  region      = "${local.project_vars.locals.gcp_region_default}"
}
EOF
}

locals {
  project_vars = read_terragrunt_config(find_in_parent_folders("project.hcl"))
}
inputs = {
  env_name   = local.project_vars.locals.env_name
  project_id = local.project_vars.locals.gcp_project_id
  region     = local.project_vars.locals.gcp_region_default
  zone       = local.project_vars.locals.gcp_zone_default

  network_name = dependency.vpc.outputs.network_name
  subnetwork   = dependency.vpc.outputs.subnets_names[0]

  ## Vault
  create_load_balancer = false
  location             = "us-central1"

  # reserve_subnet_range ?
  resource_name_prefix  = local.project_vars.locals.gcp_resources_prefix
  ssh_source_ranges     = [] # already exists in the vpc
  leader_tls_servername = "vault.${local.project_vars.locals.base_domain}"
  # ssl_certificate_name = (Use self-signed certificate for testing purposes)
  ssl_certificate_generate_self_signed = true
  # tls_secret_id        = (Optional if ssl_certificate_generate_self_signed is true)
  vault_version        = "1.13.2-1" #? apt-cache madison vault
  standalone           = false
  node_count           = 1
  vm_machine_type      = "e2-small"
  vm_disk_size         = 25
  vm_disk_source_image = "projects/ubuntu-os-cloud/global/images/family/ubuntu-2204-lts"
  vm_disk_type         = "pd-standard"
  vm_nat_ip            = "ephemeral"
}

