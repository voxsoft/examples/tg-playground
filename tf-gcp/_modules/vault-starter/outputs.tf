output "lb_address" {
  value       = var.create_load_balancer ? module.load_balancer[0].address : null
  description = "Load Balancer Address"
}
