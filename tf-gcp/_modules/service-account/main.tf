resource "google_service_account" "this" {
  project      = var.project_id
  account_id   = var.name
  display_name = var.display_name
  description  = var.description
}

resource "google_project_iam_member" "this" {
  for_each = toset(var.iam_roles)
  project  = var.project_id
  role     = each.key
  member   = "serviceAccount:${google_service_account.this.email}"
}

resource "google_service_account_iam_member" "this_wi" {
  for_each           = toset(var.wi_kubernetes_sa_paths)
  service_account_id = google_service_account.this.name
  role               = "roles/iam.workloadIdentityUser"
  member             = "serviceAccount:${var.project_id}.svc.id.goog[${each.value}]"
}

resource "google_service_account_key" "this" {
  count              = var.key_create ? 1 : 0
  service_account_id = google_service_account.this.name
  keepers            = var.key_rotation_days > 0 ? { rotation_time = time_rotating.sa_key_rotation[0].rotation_rfc3339 } : {}
}
resource "time_rotating" "sa_key_rotation" {
  count         = var.key_create && var.key_rotation_days > 0 ? 1 : 0
  rotation_days = var.key_rotation_days
}


## Outputs
output "key_b64" {
  value       = var.key_create ? google_service_account_key.this[0].private_key : null
  sensitive   = true
  description = "JSON Key of the Service Account, base64 encoded"
}
output "email" {
  value       = google_service_account.this.email
  description = "Service Account Email"
}
output "description" {
  value       = google_service_account.this.description
  description = "Service Account Description"
}
