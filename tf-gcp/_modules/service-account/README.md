## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_project_iam_member.this](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_service_account.this](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |
| [google_service_account_iam_member.this_wi](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account_iam_member) | resource |
| [google_service_account_key.this](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account_key) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_description"></a> [description](#input\_description) | Service Account Description | `string` | `"Service Account Managed by Terraform"` | no |
| <a name="input_display_name"></a> [display\_name](#input\_display\_name) | Service Account Display Name | `string` | `null` | no |
| <a name="input_iam_roles"></a> [iam\_roles](#input\_iam\_roles) | List of roles for IAM binding | `list(string)` | `[]` | no |
| <a name="input_key_create"></a> [key\_create](#input\_json\_key\_create) | Create Service Account JSON Key | `bool` | `false` | no |
| <a name="input_name"></a> [name](#input\_name) | Service Account Name to create | `string` | n/a | yes |
| <a name="input_project_id"></a> [project\_id](#input\_project\_id) | GCP Project ID | `any` | n/a | yes |
| <a name="input_wi_kubernetes_sa_paths"></a> [wi\_kubernetes\_sa\_paths](#input\_wi\_kubernetes\_sa\_paths) | Kubernetes Service Account Paths for Workload Identity. [ NAMESPACE/SA\_NAME ] | `list(string)` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_description"></a> [description](#output\_description) | Service Account Description |
| <a name="output_email"></a> [email](#output\_email) | Service Account Email |
| <a name="output_key_b64"></a> [key\_b64](#output\_key\_b64) | JSON Key of the Service Account, base64 encoded |
