variable "project_id" {
  description = "GCP Project ID"
}
variable "name" {
  type        = string
  description = "Service Account Name to create"
}
variable "display_name" {
  type        = string
  description = "Service Account Display Name"
  default     = null
}
variable "description" {
  type        = string
  description = "Service Account Description"
  default     = "Service Account Managed by Terraform"
}
variable "key_create" {
  type        = bool
  description = "Create Service Account JSON Key"
  default     = false
}
variable "key_rotation_days" {
  type        = number
  description = "Service Account JSON Key Rotation Days. Set `0` to disable rotation."
  default     = 0
}
variable "iam_roles" {
  type        = list(string)
  description = "List of roles for IAM binding"
  default     = []
}

variable "wi_kubernetes_sa_paths" {
  type        = list(string)
  description = "Kubernetes Service Account Paths for Workload Identity. [ NAMESPACE/SA_NAME ]"
  default     = []
  validation {
    condition = alltrue([
      for path in var.wi_kubernetes_sa_paths : can(regex(".*/.*", path))
    ])
    error_message = "Each string value in `wi_kubernetes_sa_paths` must be in \"<NAMESPACE>/<SA_NAME>\" format."
  }
}
