variable "project_id" {
  type        = string
  description = "Project ID"
}
variable "region" {
  type        = string
  description = "Default region for the resources"
}
variable "instance_name" {
  type        = string
  description = "CloudSQL Instance name"
}
variable "instance_sa_email" {
  type        = string
  description = "Service account email of the CloudSQL Instance"
}
variable "databases" {
  type        = list(string)
  description = "List of databases to export"
}
variable "bucket_name" {
  type        = string
  description = "Bucket name to export the database"
}
variable "schedule" {
  type        = string
  description = "Schedule for the export"
}

###* Cloud SQL Export configuration *###
// Scheduled CloudSQL export based on https://cloud.google.com/blog/topics/developers-practitioners/scheduling-cloud-sql-exports-using-cloud-functions-and-cloud-scheduler?hl=en

resource "google_storage_bucket_iam_member" "cloudsql_to_gcs_export" {
  bucket = var.bucket_name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${var.instance_sa_email}"
}

#* Cloud Function deploy block *#
# NOTE: Due to terraform limitations, zip file must be committed to repository
#       this code here is just for the reference
data "archive_file" "cloudsql_to_gcs_export" {
  output_path = "./dist/cloudsql_to_gcs_export.zip"
  source_dir  = "${path.module}/src"
  type        = "zip"
}
resource "random_id" "cloudsql_to_gcs_export" {
  byte_length = 8
}

# Upload the zip file to GCP bucket
resource "google_storage_bucket_object" "cloudsql_to_gcs_export" {
  name   = "code/cloudsql_to_gcs_export_${random_id.cloudsql_to_gcs_export.hex}.zip"
  bucket = var.bucket_name
  source = "./dist/cloudsql_to_gcs_export.zip"
}

# Create Cloud Function using the zip file as the source
# NOTE: There is no way in tf to update CF on changes, so some workaround may be with filemd5() function
resource "google_cloudfunctions2_function" "cloudsql_to_gcs_export" {
  depends_on  = [google_storage_bucket_object.cloudsql_to_gcs_export]
  location    = var.region
  name        = "cloudsql_to_gcs_export"
  description = "This function exports database to the backup bucket. Managed by Terraform."

  build_config {
    entry_point = "ProcessPubSub"
    runtime     = "go122"
    source {
      storage_source {
        bucket = google_storage_bucket_object.cloudsql_to_gcs_export.bucket
        object = google_storage_bucket_object.cloudsql_to_gcs_export.name
      }
    }
  }
  service_config {
    max_instance_count = 1
    available_cpu      = "0.083"
    available_memory   = "128Mi"
    timeout_seconds    = 180
    ingress_settings   = "ALLOW_INTERNAL_ONLY"
  }
  event_trigger {
    event_type     = "google.cloud.pubsub.topic.v1.messagePublished"
    pubsub_topic   = google_pubsub_topic.cloudsql_to_gcs_export.id
    retry_policy   = "RETRY_POLICY_DO_NOT_RETRY"
    trigger_region = var.region
  }
  labels = { managed_by = "terraform" }
}

#* Cloud Function scheduler block *#

# Pub/Sub topic to trigger export
resource "google_pubsub_topic" "cloudsql_to_gcs_export" {
  name   = "cloudsql_to_gcs_export"
  labels = { managed_by = "terraform" }
}

# Cloud Scheduler payload
locals {
  cloudsql_to_gcs_export_params = { for db in var.databases : db => {
    Db       = db
    Instance = var.instance_name
    Project  = var.project_id
    Gs       = "gs://${google_storage_bucket_object.cloudsql_to_gcs_export.bucket}/${var.instance_name}/${db}"
    }
  }
}
# Cloud Scheduler
resource "google_cloud_scheduler_job" "cloudsql_to_gcs_export" {
  for_each    = local.cloudsql_to_gcs_export_params
  region      = var.region
  depends_on  = [google_storage_bucket_object.cloudsql_to_gcs_export]
  name        = "cloudsql_to_gcs_export_${var.instance_name}_${each.key}"
  schedule    = var.schedule
  description = "Trigger exports the database at specified time. Managed by Terraform."
  pubsub_target {
    topic_name = google_pubsub_topic.cloudsql_to_gcs_export.id
    data       = base64encode(jsonencode(each.value))
  }
}
