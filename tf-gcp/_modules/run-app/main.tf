## Common data
data "google_project" "project" {}

## SA for Cloud Run with IAM role
resource "google_service_account" "run_service" {
  count        = var.service_account_email == "" ? 1 : 0
  account_id   = "sa-run-${var.service_name}"
  display_name = "sa-run-${var.service_name}"
  description  = "SA for CloudRun ${var.service_name} service. Managed by Terraform."
}
resource "google_project_iam_member" "run_service" {
  for_each = toset(local.run_service_roles)
  project  = var.project_id
  role     = each.key
  member   = "serviceAccount:${google_service_account.run_service[0].email}"
  # condition {
  #   title       = "expires_after_2019_12_31"
  #   description = "Expiring at midnight of 2019-12-31"
  #   expression  = "request.time < timestamp(\"2020-01-01T00:00:00Z\")"
  # }
}
locals {
  run_service_roles = var.service_account_email == "" ? [
    "roles/storage.objectViewer", # TODO: Restrict run_service to only the permissions needed
    "roles/editor", # TODO: Restrict run_service to only the permissions needed
  ] : []
}

## Cloud Run service
module "run_service" {
  source  = "GoogleCloudPlatform/cloud-run/google"
  version = "~> 0.6.0"

  # Required variables
  service_name           = var.service_name
  project_id             = var.project_id
  location               = var.location
  image                  = var.image
  service_account_email  = var.service_account_email == "" ? google_service_account.run_service[0].email : var.service_account_email
  container_command      = var.container_command
  argument               = var.argument
  # members                = var.members ##TODO: merge with provided value: var.protect_with_iap ? ["allUsers"] : []
  members                = var.protect_with_iap ? ["serviceAccount:service-${data.google_project.project.number}@gcp-sa-iap.iam.gserviceaccount.com"] : ["allUsers"]
  ports                  = var.ports
  env_vars               = var.env_vars
}

## HTTPS Load Balancer
module "http_lb_run_service" {
  count = var.lb ? 1 : 0
  source            = "GoogleCloudPlatform/lb-http/google//modules/serverless_negs"
  version           = "8.0.0"

  project           = var.project_id
  name              = "run-${var.service_name}"

  ssl                             = var.lb_https
  https_redirect                  = var.lb_https
  managed_ssl_certificate_domains = var.managed_ssl_certificate_domains

  backends = {
    default = {
      description = null
      groups = [
        {
          group = google_compute_region_network_endpoint_group.run_service[0].id
        }
      ]
      enable_cdn              = false
      security_policy         = null
      custom_request_headers  = null
      custom_response_headers = null

      iap_config = var.iap_config
      log_config = {
        enable      = false
        sample_rate = null
      }
      protocol         = null
      port_name        = null
      compression_mode = null
    }
  }
}
resource "google_compute_region_network_endpoint_group" "run_service" {
  count = var.lb ? 1 : 0
  provider              = google-beta
  name                  = "run-${var.service_name}-serverless-neg"
  network_endpoint_type = "SERVERLESS"
  region                = var.location
  cloud_run {
    service = module.run_service[0].service_name
  }
}
output "lb_backend_services" {
  description = "The backend service resources."
  value       = var.lb ? module.http_lb_run_service.backend_services : null
  sensitive   = true // can contain sensitive iap_config
}
output "lb_external_ip" {
  description = "The external IPv4 assigned to the global fowarding rule."
  value       = var.lb ? module.http_lb_run_service[0].external_ip : null
}

## IAM IAP
data "google_iam_policy" "iam_allowed_users" {
  count = var.iap_config.iam_allowed_users == null || var.iap_config.iam_allowed_users == [] ? 0 : 1
  binding {
    role = "roles/iap.httpsResourceAccessor"
    members = var.iap_config.iam_allowed_users
  }
}
resource "google_iap_web_backend_service_iam_policy" "iam_allowed_users" {
  count   = var.iap_config.iam_allowed_users == null || var.iap_config.iam_allowed_users == [] ? 0 : 1
  project = var.project_id
  # web_backend_service = module.http_lb_run_service.backends.default["default"].name
  #  web_backend_service = "${module.http_lb_run_service.name}-backend-default" ## TODO: get from module
  web_backend_service = "run-${var.service_name}-backend-default" ## TODO: get from module
  policy_data = data.google_iam_policy.iam_allowed_users[0].policy_data
}
