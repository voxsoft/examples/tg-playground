###* USERS | IAM *###
module "iam_user" {
  for_each = var.iam_users
  source   = "./modules/iam-users"
  project  = var.project_id

  user    = each.key
  roles   = each.value
}
