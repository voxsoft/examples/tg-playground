variable "project" {
  type = string
}
variable "user" {
  type = string
}
variable "roles" {
  type = list
}

resource "google_project_iam_member" "this" {
  for_each = toset(var.roles)
  project  = var.project
  role     = each.key
  member   = "user:${var.user}"
}
