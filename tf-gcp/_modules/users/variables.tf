variable "project_id" {
  description = "GCP Project ID"
}
variable "iam_users" {
  default     = {}
  description = "User roles. Example: {\"user@gmail.com\" = [ \"roles/editor\" ]}"
  type = map(list(string))
}
