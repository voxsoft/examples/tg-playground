variable "location" {
  type        = string
  default     = "us-central1"
  description = "The location of the repository"
}

locals {
  ## docker_registries_iam_policy is optional
  docker_registries_iam_policy = {}
  /*
  docker_registries_iam_policy = {
    "gcsproxy" = {
      "location" = var.location
      "bindings" = [
        {
          "role"    = "roles/artifactregistry.reader",
          "members" = [
            "user:oleksyy.marchenko@gmail.com"
          ]
        },
      ]
    }
  }
  */
  docker_registries = {
    "gcsproxy" = { 
      "location"    = var.location ## TODO: be able to specify several locations
      "description" = "GCS Proxy"
      # "docker_config" = { # NOTE: Current state: PREVIEW
      #   "immutable_tags" = false
      # }
    }
    "all" = {
      "location"    = var.location
      "description" = "other docker images"
    }
  }
}

resource "google_artifact_registry_repository" "docker" {
  for_each      = local.docker_registries
  location      = each.value.location
  repository_id = each.key
  description   = each.value.description
  format        = "DOCKER"
  # docker_config { # NOTE: Current state: PREVIEW
  #   immutable_tags = each.value.docker_config.immutable_tags
  # }
}

data "google_iam_policy" "docker" {
  for_each = local.docker_registries_iam_policy
  dynamic "binding" {
    for_each = each.value.bindings
    content {
      role    = binding.value.role
      members = binding.value.members
    }
  }
  # binding {
  #   role = each
  #   members = [
  #     "user:jane@example.com",
  #   ]
  # }
}

resource "google_artifact_registry_repository_iam_policy" "docker" {
  for_each    = local.docker_registries_iam_policy
  location    = each.value.location
  repository  = each.key
  policy_data = data.google_iam_policy.docker[each.key].policy_data
}
