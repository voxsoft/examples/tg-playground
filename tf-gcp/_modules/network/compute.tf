data "google_compute_zones" "available" {
  count   = var.spin_up_test_vm ? 1 : 0
  project = var.project_id
  region  = module.vpc.subnets_regions[0]
}

resource "google_compute_instance" "test" {
  count        = var.spin_up_test_vm ? 1 : 0
  name         = "${var.network_name}-test-vm"
  machine_type = "f1-micro"
  zone         = data.google_compute_zones.available[0].names[0]
  project      = var.project_id

  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2204-lts"
    }
  }

  network_interface {
    subnetwork = module.vpc.subnets_ids[0]
  }
}
