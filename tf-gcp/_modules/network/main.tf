
module "vpc" {
  source  = "terraform-google-modules/network/google"
  version = "~> 7.0"

  project_id   = var.project_id
  network_name = var.network_name
  routing_mode = var.routing_mode

  subnets          = var.subnets
  secondary_ranges = var.secondary_ranges

  routes         = var.routes
  firewall_rules = var.firewall_rules
}

resource "google_compute_address" "nat_ip" {
  count        = var.nat_enabled ? var.nat_ips_number : 0
  provider     = google-beta
  name         = "${var.network_name}-router0-ip${count.index}"
  address_type = "EXTERNAL"
  network_tier = "PREMIUM"
  region       = module.vpc.subnets_regions[0]

  labels      = { managed_by = "terraform" }
  description = "NAT external IP address. Managed by Terraform."
}

module "cloud_router_nat" {
  count   = var.nat_enabled ? 1 : 0
  source  = "terraform-google-modules/cloud-router/google"
  version = "~> 5.0"
  project = var.project_id
  name    = "${var.network_name}-router0"
  network = var.network_name
  region  = module.vpc.subnets_regions[0]

  nats = [{
    name = "${var.network_name}-router0-nat0"
    nat_ips = google_compute_address.nat_ip[*].self_link
    log_config = {
      # enable = true ## Always true for this module https://github.com/terraform-google-modules/terraform-google-cloud-router/issues/26
      filter = "ERRORS_ONLY"
    }
  }]
}
output "nat_ips" {
  value = var.nat_enabled ? google_compute_address.nat_ip[*].address : []
  description = "NAT external IP addresses. NOTE: if var.nat_ips_number < 1, Unable to list"
}

## Service Peering
resource "google_compute_global_address" "service_networking_connection" {
  count         = var.service_networking_connection ? 1 : 0
  provider      = google-beta
  name          = "${var.network_name}-svcnet-ip${count.index}"
  project       = var.project_id
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  address       = split("/", var.service_networking_cidr).0
  prefix_length = split("/", var.service_networking_cidr).1
  network       = module.vpc.network_name

  labels      = { managed_by = "terraform" }
  description = "Service peering IP address. Managed by Terraform."
}
resource "google_service_networking_connection" "service_networking_connection" {
  count                   = var.service_networking_connection ? 1 : 0
  provider                = google-beta
  network                 = module.vpc.network_name
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.service_networking_connection[0].name]
}
