## Common variables
variable "project_id" {
  type        = string
  description = "GCP project id"
}
variable "region_default" {
  default     = "us-central1"
  description = "GCP default region"
}
variable "zone_default" {
  default     = "us-central1-c"
  description = "GCP default zone. Must correspond with `region_default` variable"
}
variable "deletion_protection" {
  type        = bool
  default     = false
  description = "Whether or not to enable deletion protection for GCP resources"
}

# variable "network_self_link" {
#   type = string
#   description = "The self_link of the network from which created resources should be accessible"
#   default = null
# }

variable "app_name" {
  type        = string
  description = "Name of the application"
}

## Cloud SQL - Postgres variables
variable "postgres_instances" {
  type        = map(any)
  default     = {}
  description = "Map of Postgres instances to create. The key is the instance name, and the value is a map of instance options"
}

## GCS - Buckets variables
variable "gcs_buckets" {
  type        = map(any)
  default     = {}
  description = "Map of GCS Buckets to create. The key is the Bucket name, and the value is a map of bucket-related options"
}

## IAM - Service Account variables
variable "iam_sa" {
  type        = any
  default     = {}
  description = "Map of ServiceAccount-related options"
}

variable "export_to_gcs" {
  type        = map(any)
  default     = {}
  description = "CloudSQL to GCS export configuration"  
}
