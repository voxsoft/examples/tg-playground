module "gcs_buckets" {
  for_each      = var.gcs_buckets
  depends_on    = [module.iam_sa]
  source        = "terraform-google-modules/cloud-storage/google//modules/simple_bucket"
  version       = "4.0.0"
  name          = each.key
  project_id    = var.project_id
  force_destroy = !var.deletion_protection
  location      = lookup(each.value, "location", var.region_default)
  versioning    = lookup(each.value, "versioning", false)

  bucket_policy_only = lookup(each.value, "uniform_access", true)

  lifecycle_rules = lookup(each.value, "lifecycle_rules", [])

  custom_placement_config = lookup(each.value, "custom_placement_config", null)

  iam_members = lookup(each.value, "iam_members", [])
}

resource "google_storage_bucket_iam_member" "iam_sa_gcs_buckets" {
  depends_on = [module.gcs_buckets]
  ## for each element in var.gcs_buckets, where v.iam_sa_add is true, add IAM member
  for_each = { for k, v in var.gcs_buckets : k => v if lookup(v, "iam_sa_add", true) == true && lookup(var.iam_sa, "create", false) == true }
  bucket   = each.key
  role     = "roles/storage.objectAdmin"
  member   = "serviceAccount:${module.iam_sa[0].email}"
}

output "gcs" {
  value     = { for k, v in module.gcs_buckets : k => v }
  sensitive = false
}
