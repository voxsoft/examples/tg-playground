locals {
  iam_sa_env_vars = local.iam_sa_create == 1 ? {
    IAM_SA_EMAIL = module.iam_sa[0].email
  } : {}

  postgres_env_vars = { for k, v in module.postgres : k =>
    {
      CLOUDSQL_CONNECTION_NAME = v.instance_connection_name
      # CLOUDSQL_REPLICAS_CONNECTION_NAMES = v.replicas_instance_connection_names ## TODO: transform to string
      POSTGRES_PUBLIC_IP  = v.public_ip_address
      POSTGRES_PRIVATE_IP = v.private_ip_address
      POSTGRES_DB         = var.postgres_instances[k].db_name
      POSTGRES_USER       = var.postgres_instances[k].user_name
      POSTGRES_PASSWORD   = v.generated_user_password
    }
  }

  gcs_env_vars = { for k, v in module.gcs_buckets : k =>
    {
      GCS_BUCKET_NAME = k
      GCS_BUCKET_URL  = v.url
    }
  }

}

## TODO: Allow customize which PostgreSQL and GCS Bucket to use for environment variables
output "env_vars" {
  value       = merge(local.iam_sa_env_vars, values(local.postgres_env_vars)[0], values(local.gcs_env_vars)[0])
  sensitive   = true
  depends_on  = [ module.postgres, module.gcs_buckets, module.iam_sa ]
  description = "Contains merged prepared environment variables of GCP Services: IAM SA, PostgreSQL (first one provided), GCS Bucket (first one provided)"
}
output "app_env_vars" {
  value       = { "${var.app_name}" = merge(local.iam_sa_env_vars, values(local.postgres_env_vars)[0], values(local.gcs_env_vars)[0]) }
  sensitive   = true
  depends_on  = [ module.postgres, module.gcs_buckets, module.iam_sa ]
  description = "Contains merged prepared environment variables of GCP Services: IAM SA, PostgreSQL (first one provided), GCS Bucket (first one provided), keyed by var.app_name"
}
