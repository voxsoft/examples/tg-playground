locals {
  postgres_instances_ip_configuration = { for k, v in var.postgres_instances :
    k => can(v.ip_configuration) ? {
      authorized_networks                           = lookup(v.ip_configuration, "authorized_networks", [])
      ipv4_enabled                                  = lookup(v.ip_configuration, "ipv4_enabled", true)
      private_network                               = lookup(v.ip_configuration, "private_network", null)
      require_ssl                                   = lookup(v.ip_configuration, "require_ssl", null)
      allocated_ip_range                            = lookup(v.ip_configuration, "allocated_ip_range", null)
      enable_private_path_for_google_cloud_services = lookup(v.ip_configuration, "enable_private_path_for_google_cloud_services", false)
      } : {
      authorized_networks                           = []
      ipv4_enabled                                  = true
      private_network                               = null
      require_ssl                                   = null
      allocated_ip_range                            = null
      enable_private_path_for_google_cloud_services = false
    }
  }

  postgres_instances_backup_configuration = { for k, v in var.postgres_instances :
    k => can(v.backup_configuration) ? {
      enabled                        = lookup(v.backup_configuration, "enabled", false)
      start_time                     = lookup(v.backup_configuration, "start_time", "06:00")
      location                       = lookup(v.backup_configuration, "location", var.region_default)
      point_in_time_recovery_enabled = lookup(v.backup_configuration, "point_in_time_recovery_enabled", false)
      retained_backups               = lookup(v.backup_configuration, "retained_backups", 7)
      retention_unit                 = lookup(v.backup_configuration, "retention_unit", "COUNT")
      transaction_log_retention_days = lookup(v.backup_configuration, "transaction_log_retention_days", null)
      } : {
      enabled                        = false
      start_time                     = "06:00"
      location                       = null
      point_in_time_recovery_enabled = false
      retained_backups               = 7
      retention_unit                 = "COUNT"
      transaction_log_retention_days = null
    }
  }
}

module "postgres" {
  for_each            = var.postgres_instances
  source              = "GoogleCloudPlatform/sql-db/google//modules/postgresql"
  version             = "15.0.0"
  deletion_protection = var.deletion_protection
  create_timeout      = "35m"

  project_id = var.project_id
  region     = var.region_default
  zone       = var.zone_default
  name       = each.key
  db_name    = lookup(each.value, "db_name", each.key)

  additional_databases = lookup(each.value, "additional_databases", []) 
  additional_users = lookup(each.value, "additional_users", []) 
  user_name        = lookup(each.value, "user_name", each.key)
  database_version = lookup(each.value, "database_version", "POSTGRES_15")
  tier             = lookup(each.value, "tier", "db-f1-micro")

  availability_type = lookup(each.value, "availability_type", null)

  ip_configuration = local.postgres_instances_ip_configuration[each.key]

  backup_configuration = local.postgres_instances_backup_configuration[each.key]

  # read_replicas = lookup(each.value, "read_replicas", null)

  user_labels = { managed_by = "terraform" }
}

## Export to GCS (WARNING: Hardcode)
module "cloudsql_to_gcs_export" {
  for_each = var.postgres_instances
  source   = "../cloudsql-to-gcs-export"

  project_id = var.project_id
  region     = var.region_default

  instance_name     = module.postgres[each.key].instance_name
  instance_sa_email = module.postgres[each.key].instance_service_account_email_address
  bucket_name       = each.value.export_to_gcs.bucket_name
  databases         = each.value.export_to_gcs.databases
  schedule          = each.value.export_to_gcs.schedule
}

output "postgres" {
  value     = { for k, v in module.postgres : k => v }
  sensitive = true
  depends_on  = [ module.postgres ]
}
