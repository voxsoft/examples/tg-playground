locals {
  iam_sa_create = lookup(var.iam_sa, "create", false) == true ? 1 : 0
}

module "iam_sa" {
  count                  = local.iam_sa_create
  source                 = "../service-account"
  project_id             = var.project_id
  name                   = var.iam_sa.name
  display_name           = lookup(var.iam_sa, "display_name", null)
  description            = lookup(var.iam_sa, "description", null)
  key_create             = lookup(var.iam_sa, "key_create", false)
  iam_roles              = lookup(var.iam_sa, "roles", [])
  wi_kubernetes_sa_paths = lookup(var.iam_sa, "workload_identity_paths", [])
}

output "iam_sa" {
  value     = { for k, v in module.iam_sa : k => v }
  sensitive = true
}
