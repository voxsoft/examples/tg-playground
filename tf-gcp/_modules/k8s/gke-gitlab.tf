###* Gitlab Service Account *###
resource "kubernetes_namespace" "gitlab" {
  count = var.gitlab_sa_create || var.gitlab_agent_create ? 1 : 0
  metadata {
    name   = "gitlab"
    labels = { managed_by = "terraform" }
  }
}


### TODO: Move to universal module to create service accounts
# Service account
resource "kubernetes_service_account" "gitlab_admin" {
  count      = var.gitlab_sa_create ? 1 : 0
  depends_on = [kubernetes_namespace.gitlab]
  metadata {
    name      = "gitlab-admin"
    namespace = "gitlab"
    labels    = { managed_by = "terraform" }
  }
}
# Service account secret
data "kubernetes_secret" "gitlab_admin" {
  count      = var.gitlab_sa_create ? 1 : 0
  depends_on = [kubernetes_service_account.gitlab_admin]
  metadata {
    name      = kubernetes_service_account.gitlab_admin[0].default_secret_name
    namespace = "gitlab"
    labels    = { managed_by = "terraform" }
  }
}
# Service account RoleBinding
resource "kubernetes_cluster_role_binding" "gitlab_admin" {
  count      = var.gitlab_sa_create ? 1 : 0
  depends_on = [kubernetes_service_account.gitlab_admin]
  metadata {
    name   = "gitlab-admin"
    labels = { managed_by = "terraform" }
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "gitlab-admin"
    namespace = "gitlab"
  }
}

# Service account outputs
output "kubernetes_api_url" {
  value = var.gitlab_sa_create ? "https://${data.google_container_cluster.cluster0.endpoint}" : null
}
output "kubernetes_ca_certificate" {
  value     = var.gitlab_sa_create ? base64decode(data.google_container_cluster.cluster0.master_auth.0.cluster_ca_certificate) : null
  sensitive = true
}
output "gitlab_sa_token" {
  value     = var.gitlab_sa_create ? data.kubernetes_secret.gitlab_admin[0].data.token : null
  sensitive = true
}

locals {
  gitlab_kubeconfig = !var.gitlab_sa_create ? 0 : <<EOF
apiVersion: v1
kind: Config
users:
- name: gitlab-admin
  user:
    token: ${data.kubernetes_secret.gitlab_admin[0].data.token}
clusters:
- cluster:
    certificate-authority-data: ${data.google_container_cluster.cluster0.master_auth.0.cluster_ca_certificate}
    server: https://${data.google_container_cluster.cluster0.endpoint}
  name: ${var.gke_cluster_name}
contexts:
- context:
    cluster: ${var.gke_cluster_name}
    user: gitlab-admin
  name: ${var.gke_cluster_name}
current-context: ${var.gke_cluster_name}
EOF
}
output "gitlab_kubeconfig" { # Kubernetes | Gitlab | Kubeconfig (can be set as gitlab group variable)
  value     = var.gitlab_sa_create ? local.gitlab_kubeconfig : null
  sensitive = true
}
