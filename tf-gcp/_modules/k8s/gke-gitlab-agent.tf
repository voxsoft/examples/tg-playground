###* Gitlab Kubernetes Agent *###
## TODO: Move to gitlab.tf

resource "helm_release" "gitlab_agent" {
  count      = var.ingress_create ? 1 : 0
  depends_on = [kubernetes_namespace.gitlab]
  name       = "gitlab-agent"
  namespace  = kubernetes_namespace.gitlab[0].metadata[0].name
  wait       = true

  chart      = "gitlab-agent"
  repository = "https://charts.gitlab.io"
  version    = "1.14.1"
  timeout    = "300"
  set {
    name  = "image.tag"
    value = "v16.1.2" ## TODO: Remove hardcoded version
  }
  set {
    name  = "config.token"
    value = var.gitlab_agent_token
  }
}


/*
# Manifests 
resource "kubernetes_secret" "gitlab_kubernetes_agent_token" {
  count      = var.gitlab_agent_create ? 1 : 0
  depends_on = [kubernetes_namespace.gitlab]
  metadata {
    name      = "gitlab-kubernetes-agent-token"
    namespace = "gitlab"
    labels    = { managed_by = "terraform" }
  }
  type = "Opaque"
  data = {
    token = var.gitlab_agent_token
  }
  lifecycle {
    ignore_changes = [metadata[0].annotations["encryption-key-rotation-time"]]
  }
}

resource "kubernetes_service_account" "gitlab_kubernetes_agent" {
  count      = var.gitlab_agent_create ? 1 : 0
  depends_on = [kubernetes_namespace.gitlab]
  metadata {
    name      = "gitlab-kubernetes-agent"
    namespace = "gitlab"
    labels    = { managed_by = "terraform" }
  }
}

resource "kubernetes_deployment" "gitlab_kubernetes_agent" {
  count            = var.gitlab_agent_create ? 1 : 0
  depends_on       = [kubernetes_namespace.gitlab, kubernetes_service_account.gitlab_kubernetes_agent, kubernetes_secret.gitlab_kubernetes_agent_token]
  wait_for_rollout = false
  metadata {
    name      = "gitlab-kubernetes-agent"
    namespace = "gitlab"
    labels    = { managed_by = "terraform" }
  }
  spec {
    replicas = "1"
    selector {
      match_labels = {
        "app" = "gitlab-kubernetes-agent"
      }
    }
    strategy {
      type = "RollingUpdate"
      rolling_update {
        max_surge       = "0"
        max_unavailable = "1"
      }
    }

    template {
      metadata {
        annotations = {
          "prometheus.io/path"   = "/metrics"
          "prometheus.io/port"   = "8080"
          "prometheus.io/scrape" = "true"
        }
        labels = {
          "app" = "gitlab-kubernetes-agent"
        }
      }
      spec {
        service_account_name            = "gitlab-kubernetes-agent"
        automount_service_account_token = true
        enable_service_links            = false

        container {
          name  = "agent"
          image = "registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/agentk:stable" #v14.9.1
          args = [
            "--token-file=/config/token",
            "--kas-address",
            "wss://kas.gitlab.com",
          ]
          env {
            name = "POD_NAMESPACE"
            value_from {
              field_ref {
                field_path = "metadata.namespace"
              }
            }
          }
          env {
            name = "POD_NAME"
            value_from {
              field_ref {
                field_path = "metadata.name"
              }
            }
          }

          liveness_probe {
            http_get {
              path = "/liveness"
              port = "8080"
            }
            initial_delay_seconds = 15
            period_seconds        = 20
          }

          readiness_probe {
            http_get {
              path = "/readiness"
              port = "8080"
            }
            initial_delay_seconds = 5
            period_seconds        = 10
          }
          volume_mount {
            mount_path = "/config"
            name       = "token-volume"
            read_only  = false
          }
        }

        volume {
          name = "token-volume"
          secret {
            default_mode = "0644"
            optional     = false
            secret_name  = "gitlab-kubernetes-agent-token"
          }
        }
      }
    }
  }

}

#! Simple RBAC
# Service account RoleBinding
resource "kubernetes_cluster_role_binding" "gitlab_kubernetes_agent" {
  count      = var.gitlab_agent_create ? 1 : 0
  depends_on = [kubernetes_service_account.gitlab_kubernetes_agent]
  metadata {
    name   = "gitlab-kubernetes-agent-admin"
    labels = { managed_by = "terraform" }
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "gitlab-kubernetes-agent"
    namespace = "gitlab"
  }
}
*/

/*
#! Advanced RBAC 
resource "kubernetes_cluster_role" "gitlab_kubernetes_agent_write" {
  count      = var.gitlab_agent_create ? 1 : 0
  depends_on = [kubernetes_namespace.gitlab, kubernetes_service_account.gitlab_kubernetes_agent]
  metadata {
    name = "gitlab-kubernetes-agent-write"
  }
  rule {
    non_resource_urls = []
    resource_names    = []
    api_groups        = ["*"]
    resources         = ["*"]
    verbs = [
      "create",
      "update",
      "delete",
      "patch",
    ]
  }
}
resource "kubernetes_cluster_role_binding" "gitlab_kubernetes_agent_write_binding" {
  count      = var.gitlab_agent_create ? 1 : 0
  depends_on = [kubernetes_namespace.gitlab, kubernetes_service_account.gitlab_kubernetes_agent]
  metadata {
    name   = "gitlab-kubernetes-agent-write-binding"
    labels = { managed_by = "terraform" }
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "gitlab-kubernetes-agent-write"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "gitlab-kubernetes-agent"
    namespace = "gitlab"
  }
}

resource "kubernetes_cluster_role" "gitlab_kubernetes_agent_read" {
  count      = var.gitlab_agent_create ? 1 : 0
  depends_on = [kubernetes_namespace.gitlab, kubernetes_service_account.gitlab_kubernetes_agent]
  metadata {
    name   = "gitlab-kubernetes-agent-read"
    labels = { managed_by = "terraform" }
  }
  rule {
    non_resource_urls = []
    resource_names    = []
    api_groups        = ["*"]
    resources         = ["*"]
    verbs = [
      "get",
      "list",
      "watch",
    ]
  }
}
resource "kubernetes_cluster_role_binding" "gitlab_kubernetes_agent_read_binding" {
  count      = var.gitlab_agent_create ? 1 : 0
  depends_on = [kubernetes_namespace.gitlab, kubernetes_service_account.gitlab_kubernetes_agent]
  metadata {
    name   = "gitlab-kubernetes-agent-read-binding"
    labels = { managed_by = "terraform" }
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "gitlab-kubernetes-agent-read"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "gitlab-kubernetes-agent"
    namespace = "gitlab"
  }
}
*/
