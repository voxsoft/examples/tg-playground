###* Terraform backend configuration *###
###* Main Providers *###
/*
provider "google" {
  # credentials = "sa-terraform.json"
  project     = var.project_id
  region      = var.region_default
}
provider "google-beta" {
  # credentials = "sa-terraform.json"
  project     = var.project_id
  region      = var.region_default
}
*/

###* GKE Providers *###
// Current client data
data "google_client_config" "current" {}

// Cluster data
data "google_container_cluster" "cluster0" {
  name     = var.gke_cluster_name
  location = var.gke_cluster_regional ? var.region_default : var.zone_default
}

// Provider | Kubernetes
provider "kubernetes" {
  host                   = "https://${data.google_container_cluster.cluster0.endpoint}"
  token                  = data.google_client_config.current.access_token
  client_certificate     = base64decode(data.google_container_cluster.cluster0.master_auth.0.client_certificate)
  client_key             = base64decode(data.google_container_cluster.cluster0.master_auth.0.client_key)
  cluster_ca_certificate = base64decode(data.google_container_cluster.cluster0.master_auth.0.cluster_ca_certificate)
}

// Provider | Helm
provider "helm" {
  kubernetes {
    host                   = "https://${data.google_container_cluster.cluster0.endpoint}"
    token                  = data.google_client_config.current.access_token
    client_certificate     = base64decode(data.google_container_cluster.cluster0.master_auth.0.client_certificate)
    client_key             = base64decode(data.google_container_cluster.cluster0.master_auth.0.client_key)
    cluster_ca_certificate = base64decode(data.google_container_cluster.cluster0.master_auth.0.cluster_ca_certificate)
  }
}
