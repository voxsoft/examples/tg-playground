resource "kubernetes_namespace" "kube_prometheus_stack" {
  count = var.prom_stack_create ? 1 : 0
  metadata {
    name   = "kube-prometheus-stack"
    labels = { managed_by = "terraform" }
  }
}

resource "helm_release" "kube_prometheus_stack" {
  count      = var.prom_stack_create ? 1 : 0
  name       = "kube-prometheus-stack"
  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "kube-prometheus-stack"
  namespace  = kubernetes_namespace.kube_prometheus_stack[0].metadata[0].name
  version    = "56.21.1"
  timeout    = "450"
  values = [
    <<-EOF
    prometheusOperator:
      admissionWebhooks: {enabled: false} ## GKE private cluster specific
      tls: {enabled: false}               ## GKE private cluster specific
    grafana:
      ingress:
        enabled: true
        annotations:
          kubernetes.io/ingress.class: nginx
          cert-manager.io/cluster-issuer: letsencrypt-http
        hosts:
          - ${var.prom_stack_grafana_host}
        tls:
          - secretName: ${var.prom_stack_grafana_host}-tls
            hosts:
              - ${var.prom_stack_grafana_host}
    EOF
  ]
}
