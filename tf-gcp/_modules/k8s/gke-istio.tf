resource "kubernetes_namespace" "istio" {
  count = var.istio_create ? 1 : 0
  metadata {
    name = "istio-system"
  }
}
resource "helm_release" "istio_base" {
  count      = var.istio_create ? 1 : 0
  depends_on = [kubernetes_namespace.istio]
  name       = "base"
  repository = "https://istio-release.storage.googleapis.com/charts"
  chart      = "base"
  namespace  = kubernetes_namespace.istio[0].metadata[0].name
  version    = "1.18.0"
  values = [ <<-EOF
global:
  istioNamespace: ${kubernetes_namespace.istio[0].metadata[0].name}
EOF
  ]
}
resource "helm_release" "istiod" {
  count      = var.istio_create ? 1 : 0
  depends_on = [helm_release.istio_base]
  name             = "istiod"
  repository       = "https://istio-release.storage.googleapis.com/charts"
  chart            = "istiod"
  namespace        = kubernetes_namespace.istio[0].metadata[0].name
  version          = helm_release.istio_base[0].version
  values = [ <<-EOF
pilot:
  resources:
    requests: ## Sandbox values
      cpu: 100m
      memory: 500Mi
telemetry:
  enabled: false
EOF
  ]
}

resource "kubernetes_namespace" "istio_ingress" {
  count = var.istio_create ? 1 : 0
  metadata {
    name = "ingress-istio"
  }
}
## Extra steps for GKE required https://istio.io/latest/docs/setup/platform-setup/gke/
resource "helm_release" "istio_gateway" {
  count      = var.istio_create ? 1 : 0
  depends_on = [kubernetes_namespace.istio_ingress, helm_release.istiod]
  name             = "gateway"
  repository       = "https://istio-release.storage.googleapis.com/charts"
  chart            = "gateway"
  namespace        = kubernetes_namespace.istio_ingress[0].metadata[0].name
  version          = helm_release.istio_base[0].version
}
