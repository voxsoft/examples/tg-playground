# SA that will use Workload Identity
/*
resource "kubernetes_service_account" "sa_coreapi" {
  count = 0
  depends_on = [kubernetes_namespace.main]
  metadata {
    name      = "coreapi"
    namespace = kubernetes_namespace.main.metadata[0].name
    annotations = {
      "iam.gke.io/gcp-service-account" = "sa-coreapi@${var.project_id}.iam.gserviceaccount.com"
    }
    labels = { managed_by = "terraform" }
  }
}

### Because of a bug in Python SDK - SA JSON is required to generate signed GCS url
resource "kubernetes_secret" "sa_coreapi" {
  count = 0
  depends_on = [kubernetes_namespace.main]
  metadata {
    name      = "coreapi-sa-json"
    namespace = kubernetes_namespace.main.metadata[0].name
    labels    = { managed_by = "terraform" }
  }

  binary_data = {
    sa-key-json = data.terraform_remote_state.gcp.outputs.sa_coreapi.json_key_b64
  }
  type = "Opaque"
  lifecycle {
    ignore_changes = [metadata[0].annotations["encryption-key-rotation-time"]]
  }
}
*/
