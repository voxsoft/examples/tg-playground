resource "kubernetes_namespace" "argocd" {
  count = var.argocd_create ? 1 : 0
  metadata {
    name = "argocd"
  }
}
resource "helm_release" "argocd" {
  count      = var.argocd_create ? 1 : 0
  depends_on = [kubernetes_namespace.argocd]
  name       = "argocd"
  repository = "https://argoproj.github.io/argo-helm"
  chart      = "argo-cd"
  timeout    = 180
  namespace  = kubernetes_namespace.argocd[0].metadata[0].name
  atomic     = true
  version    = "7.4.4" ## ArgoCD v2.12.1
  values = [
    <<-EOF
global:
  logging:
    format: json
server:
  extraArgs:
    - --insecure ## This will allow to use it with nginx ingress
  ingress:
    enabled: true
    annotations:
      kubernetes.io/ingress.class: nginx
      cert-manager.io/cluster-issuer: letsencrypt-http
    hostname: ${var.argocd_host}
    tls: true
## The following is required to use the helm-secrets plugin
configs:
  cm:
    ui.bannercontent: "VOXSOFT ArgoCD"
    ui.bannerurl: "https://gitlab.com/voxsoft"
    ui.bannerpermanent: "true"
    ui.bannerposition: "top"
    helm.valuesFileSchemes: >-
      secrets+gpg-import, secrets+gpg-import-kubernetes,
      secrets+age-import, secrets+age-import-kubernetes,
      secrets,secrets+literal,
      https
repoServer:
  serviceAccount:
    annotations:
      iam.gke.io/gcp-service-account: "sa-argocd-repo-server@${var.project_id}.iam.gserviceaccount.com"
  env:
    - name: HELM_PLUGINS
      value: /custom-tools/helm-plugins/
    - name: HELM_SECRETS_CURL_PATH
      value: /custom-tools/curl
    - name: HELM_SECRETS_SOPS_PATH
      value: /custom-tools/sops
    - name: HELM_SECRETS_VALS_PATH
      value: /custom-tools/vals
    - name: HELM_SECRETS_KUBECTL_PATH
      value: /custom-tools/kubectl
    - name: HELM_SECRETS_BACKEND
      value: sops
    ## CHeck for more restrictive settings at
    ## https://github.com/jkroepke/helm-secrets/wiki/Security-in-shared-environments
    - name: HELM_SECRETS_VALUES_ALLOW_ABSOLUTE_PATH
      value: "true" ## This is required for Multi-Source Application Support
    - name: HELM_SECRETS_WRAPPER_ENABLED
      value: "true" ## This is required for Multi-Source Application Support
    - name: HELM_SECRETS_VALUES_ALLOW_PATH_TRAVERSAL
      value: "true" ## This is required for ../ dir support for helm values
    - name: HELM_SECRETS_DECRYPT_SECRETS_IN_TMP_DIR
      value: "true"
    - name: HELM_SECRETS_HELM_PATH
      value: /usr/local/bin/helm
  volumes:
    - name: custom-tools
      emptyDir: {}
  volumeMounts:
    - mountPath: /custom-tools
      name: custom-tools
    - mountPath: /usr/local/sbin/helm
      subPath: helm
      name: custom-tools
  initContainers:
    - name: download-tools
      image: alpine:latest
      imagePullPolicy: IfNotPresent
      command: [sh, -ec]
      env:
        - name: HELM_SECRETS_VERSION
          value: "4.6.1"
        - name: KUBECTL_VERSION
          value: "1.30.3"
        - name: SOPS_VERSION
          value: "3.9.0"
      args:
        - |
          mkdir -p /custom-tools/helm-plugins
          wget -qO- https://github.com/jkroepke/helm-secrets/releases/download/v$${HELM_SECRETS_VERSION}/helm-secrets.tar.gz | tar -C /custom-tools/helm-plugins -xzf-;
          
          wget -qO /custom-tools/curl https://github.com/moparisthebest/static-curl/releases/latest/download/curl-amd64
          wget -qO /custom-tools/sops https://github.com/getsops/sops/releases/download/v$${SOPS_VERSION}/sops-v$${SOPS_VERSION}.linux.amd64
          wget -qO /custom-tools/kubectl https://dl.k8s.io/release/v$${KUBECTL_VERSION}/bin/linux/amd64/kubectl

          cp /custom-tools/helm-plugins/helm-secrets/scripts/wrapper/helm.sh /custom-tools/helm
          
          chmod +x /custom-tools/*
      volumeMounts:
        - mountPath: /custom-tools
          name: custom-tools
EOF
  ]
}

## SSH key for ArgoCD repository access
resource "tls_private_key" "argocd_ssh_key" {
  count     = var.argocd_ssh_key_create ? 1 : 0
  algorithm = "RSA"
  rsa_bits  = 4096
}
output "argocd_ssh_key_pub" {
  value = tls_private_key.argocd_ssh_key[0].public_key_openssh
}

## Store SSH key in secret
resource "kubernetes_secret" "argocd_ssh_key" {
  for_each = var.argocd_create ? toset(var.argocd_ssh_key_repositories) : []
  metadata {
    name      = "argocd-ssh-key"
    namespace = kubernetes_namespace.argocd[0].metadata[0].name
    labels = {
      "argocd.argoproj.io/secret-type" = "repository"
    }
  }
  data = {
    type          = "git"
    url           = each.key
    sshPrivateKey = tls_private_key.argocd_ssh_key[0].private_key_pem
  }
  type = "Opaque"
}
