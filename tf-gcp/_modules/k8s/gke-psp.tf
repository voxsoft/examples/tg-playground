###* Pod Security Policies *###
## Deprecated! 
## TODO: Move to implementation
resource "kubernetes_pod_security_policy" "psp_default" {
  count = 0
  metadata {
    name = "default"
    labels = {
      managed_by = "terraform"
    }
  }
  spec {
    # allowed_proc_mount_types           = []
    # allowed_unsafe_sysctls             = []
    # default_add_capabilities           = []
    # forbidden_sysctls                  = []
    # required_drop_capabilities         = []
    allow_privilege_escalation         = false
    default_allow_privilege_escalation = false
    host_ipc                           = false
    host_network                       = false
    host_pid                           = false
    privileged                         = false
    read_only_root_filesystem          = false

    allowed_capabilities = ["*"]

    volumes = [
      "persistentVolumeClaim",
      "configMap",
      "secret",
      "emptyDir",
      "downwardAPI",
      "projected",
    ]

    fs_group {
      rule = "RunAsAny"
    }
    run_as_user {
      rule = "RunAsAny"
    }
    se_linux {
      rule = "RunAsAny"
    }
    supplemental_groups {
      rule = "RunAsAny"
    }
  }
}

resource "kubernetes_cluster_role" "psp_default" {
  count = 0
  metadata {
    name = "psp-default"
    labels = {
      managed_by = "terraform"
    }
  }
  rule {
    api_groups     = ["extensions"]
    resources      = ["podsecuritypolicies"]
    resource_names = ["default"]
    verbs          = ["use"]
  }
}

resource "kubernetes_cluster_role_binding" "psp_default" {
  depends_on = [kubernetes_cluster_role.psp_default, kubernetes_pod_security_policy.psp_default]
  count      = 0
  metadata {
    name = "psp-default"
    labels = {
      managed_by = "terraform"
    }
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "psp-default"
  }
  subject {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Group"
    name      = "system:serviceaccounts"
    namespace = "kube-system"
  }
}
