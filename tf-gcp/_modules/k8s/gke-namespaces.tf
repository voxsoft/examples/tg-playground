###* Common | Namespaces *###
resource "kubernetes_namespace" "namespaces" {
  for_each = toset(var.namespaces)
  metadata {
    name   = each.key
    labels = { managed_by = "terraform" }
  }
}

## Registry credentials
/*
resource "kubernetes_secret" "registry" {
  for_each = toset(var.namespaces)
  metadata {
    name      = "registry-auth"
    namespace = each.key
  }
  type = "kubernetes.io/dockerconfigjson"
  data = {
    ".dockerconfigjson" = jsonencode({
      "auths" : {
        "${var.registry_url}" : {
          username = var.registry_username
          password = var.registry_password
        }
      }
    })
  }
}

resource "kubernetes_default_service_account" "default" {
  for_each = toset(var.namespaces)
  metadata {
    namespace = each.key
    annotations = {
      "iam.gke.io/gcp-service-account" = "SERVICE_ACCOUNT@${var.project_id}.iam.gserviceaccount.com"
    }
  }
  image_pull_secret {
    name = kubernetes_secret.registry.metadata.0.name
  }
  lifecycle {
    ignore_changes = [secret]
  }
}
*/
