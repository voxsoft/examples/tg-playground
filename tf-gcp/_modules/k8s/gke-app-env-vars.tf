# Can be used as environment variables to configure microservices (GCS, SQL, Redis, etc.) 
resource "kubernetes_secret" "app_env_vars" {
  for_each   = var.app_env_vars
  depends_on = [ kubernetes_namespace.namespaces ]
  metadata {
    name      = "${each.key}-gcp-env"
    namespace = kubernetes_namespace.namespaces[element(var.namespaces,0)].metadata[0].name
    labels    = { managed_by = "terraform" }
  }
  data = tomap(each.value)
  type = "Opaque"
}
