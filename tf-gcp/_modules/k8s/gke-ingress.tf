

###* Kubernetes Ingress *###
resource "kubernetes_namespace" "ingress_nginx" {
  count = var.ingress_create ? 1 : 0
  metadata {
    name   = "ingress-nginx"
    labels = { managed_by = "terraform" }
  }
}

resource "helm_release" "ingress_nginx" {
  count      = var.ingress_create ? 1 : 0
  depends_on = [kubernetes_namespace.ingress_nginx]
  name       = "ingress-nginx"
  namespace  = kubernetes_namespace.ingress_nginx[0].metadata[0].name
  wait       = true

  chart      = "ingress-nginx"
  repository = "https://kubernetes.github.io/ingress-nginx"
  version    = "4.10.0"
  timeout    = "300"
  values = [
    <<-EOF
controller:
  ingressClass: nginx
  config:
    use-gzip: "true"
    gzip-level: "1"
    # log-format-escape-json: "true"
    # log-format-upstream: '{"timestamp":"$time_iso8601", "requestID":"$req_id", "proxyUpstreamName":"$proxy_upstream_name",
    #   "proxyAlternativeUpstreamName":"$proxy_alternative_upstream_name", "upstreamStatus":"$upstream_status", "upstreamAddr":"$upstream_addr",
    #   "httpRequest":{"requestMethod":"$request_method", "requestUrl":"$host$request_uri", "status":"$status",
    #   "requestSize":"$request_length", "responseSize":"$upstream_response_length", "userAgent":"$http_user_agent",
    #   "remoteIp":"$remote_addr", "referer":"$http_referer", "latency":"$upstream_response_time", "protocol":"$server_protocol"}}'
  replicaCount: 2
  autoscaling:
    enabled: true
    minReplicas: 1
    maxReplicas: 2
    targetCPUUtilizationPercentage: 600
    targetMemoryUtilizationPercentage: 100
  service:
    loadBalancerIP: ${var.ingress_ips[0]}
  affinity:
    podAntiAffinity:
      preferredDuringSchedulingIgnoredDuringExecution:
      - weight: 100
        podAffinityTerm:
          topologyKey: "kubernetes.io/hostname"
          labelSelector:
            matchExpressions:
            - key: "app.kubernetes.io/component"
              operator: In
              values:
                - controller
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: "workload-type"
            operator: In
            values: ["application"]
  resources:
    requests:
      cpu: 100m
      memory: 250Mi
    limits:
      cpu: 750m
  # metrics:
  #   enabled: true
  #   serviceMonitor:
  #     enabled: true
  #     additionalLabels:
  #       release: "prometheus-operator"
defaultBackend:
  enabled: false
  affinity:
    nodeAffinity:
      preferredDuringSchedulingIgnoredDuringExecution:
      - weight: 50
        preference:
          matchExpressions:
          - key: "workload-type"
            operator: In
            values: ["application"]
  resources:
    requests:
      cpu: 10m
      memory: 20Mi
    limits:
      cpu: 100m
      memory: 125Mi

EOF
  ]
}

# Load Balancer output
#? In case of ephemeral/dynamic IP this will get assigned IP to LB
data "kubernetes_service" "ingress_nginx" {
  count      = var.ingress_create && var.ingress_ips == [] ? 1 : 0
  depends_on = [helm_release.ingress_nginx]
  metadata {
    name      = "ingress-nginx-controller"
    namespace = helm_release.ingress_nginx[0].namespace
  }
}

output "ingress_nginx_loadbalancer_ip" {
  value = var.ingress_create && var.ingress_ips == [] ? data.kubernetes_service.ingress_nginx[0].status[0].load_balancer[0].ingress[0].ip : null
}
