###* InfluxDB *###
###* InfluxDB as TSDB for telemetry data for query for analytics *###
###* Deprecated, Disabled

/*
resource "kubernetes_namespace" "influxdb" {
  metadata {
    name   = "influxdb"
    labels = { managed_by = "terraform" }
  }
}

resource "random_password" "influxdb_admin_password" {
  length  = 16
  special = false
}
resource "random_password" "influxdb_admin_token" {
  length  = 24
  special = false
}

resource "helm_release" "influxdb" {
  depends_on = [kubernetes_namespace.influxdb]

  name       = "influxdb"
  repository = "https://helm.influxdata.com"
  chart      = "influxdb2"
  namespace  = kubernetes_namespace.influxdb.metadata[0].name
  version    = "2.0.4" #v2.1.1
  timeout    = "300"
  values = [
    <<-EOF
resources:
  limits:
    cpu: 1000m
    memory: 4000Mi
  requests:
    cpu: 100m
    memory: 500Mi
adminUser:
  user: "admin"
  password: "${random_password.influxdb_admin_password.result}"
  token: "${random_password.influxdb_admin_token.result}"
persistence:
  size: 50Gi
  storageClass: standard-rwo
env:
  - name: INFLUXD_LOG_LEVEL
    value: info
service:
  type: LoadBalancer
  # loadBalancerIP: data.terraform_remote_state.gcp.outputs.gke0_influxdb_ip
  annotations:
    cloud.google.com/load-balancer-type: "Internal"
  #? Option:
  # type: ClusterIP
  # annotations:
  #   cloud.google.com/neg: "{\"ingress\": true}"
#? Option
ingress: 
  enabled: false
  tls: false
  # ingressClassName: gce-internal
  hostname: influxdb.ipe.internal
  annotations:
    kubernetes.io/ingress.class: gce-internal

EOF
  ]
}

# /* #? In case of ephemeral/dynamic IP this will get assigned IP to LB
# TBD: Pre-create IP in tf-gcp
data "kubernetes_service" "influxdb_lb" {
  depends_on = [helm_release.influxdb] # Always triggers data refresh
  metadata {
    name      = "${helm_release.influxdb.name}-influxdb2"
    namespace = helm_release.influxdb.namespace
  }
}


output "influxdb" {
  value = {
    INFLUXDB_URL      = "http://${data.kubernetes_service.influxdb_lb.status.0.load_balancer.0.ingress.0.ip}",
    INFLUXDB_USER     = "admin",
    INFLUXDB_PASSWORD = random_password.influxdb_admin_password.result,
    INFLUXDB_TOKEN    = random_password.influxdb_admin_token.result,
  }
  sensitive = true
}

module "influxdb_setup" {
  depends_on = [helm_release.influxdb]
  source = "./modules/influxdb-setup"
  namespace = kubernetes_namespace.influxdb.metadata[0].name
}
*/
