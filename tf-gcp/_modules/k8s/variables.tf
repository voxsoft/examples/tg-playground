
# Global Settings
variable "env_name" {
  default     = "primer"
  description = "Environment name"
}
variable "project_id" {
  description = "GCP project id"
}
variable "region_default" {
  default     = "us-central1"
  description = "GCP default region"
}
variable "zone_default" {
  default     = "us-central1-c"
  description = "GCP default zone. Must correspond with region_default"
}
variable "resources_ha" {
  type        = bool
  default     = false
  description = "Create Highly Available resources in GKE when possible"
}

# GKE/Kubernetes Settings
variable "gke_cluster_name" {
  description = "Name of the GKE cluster"
}
variable "gke_cluster_regional" {
  default     = false
  description = "Communicate with regional GKE Cluster if `true`, otherwise - zonal"
}
variable "namespaces" {
  default     = ["main"]
  description = "List of Kubernetes namespaces that will be created"
}

# Misc Settings

variable "gitlab_sa_create" {
  type        = bool
  default     = false
  description = "Create ServiceAccount for Gitlab"
}
variable "gitlab_agent_create" {
  type        = bool
  default     = false
  description = "Install Gitlab Agent https://docs.gitlab.com/ee/user/clusters/agent/"
}
variable "gitlab_agent_token" {
  sensitive   = true
  type        = string
  default     = ""
  description = "Gitlab Agent Token"
}

# variable "base_domain" {
#   default     = null
#   description = "Default base domain for external endpoints"
# }


variable "ingress_create" {
  default     = false
  description = "Create Ingress Controller `ingress-nginx`"
}
variable "ingress_ips" {
  type        = list(string)
  default     = []
  description = "IP addresses to be used in the Load Balancer Service for Ingress"
}
variable "cert_manager_create" {
  default     = false
  description = "Deploy Cert-Manager"
}
variable "cert_manager_issuer_create" {
  default     = false
  description = "Create Cert-Manager Cluster Issuer. You may want to disable this until cert-manager install its CRDs"
}
variable "admin_email" {
  default     = null
  description = "Admin email for Let's Encrypt. Required if `cert_manager_create` is set to `true`"
}
variable "argocd_create" {
  description = "Create ArgoCD deployment"
  type        = bool
  default     = false
}
variable "argocd_host" {
  description = "Hostname for argocd (will be utilised in ingress if enabled)"
  type        = string
  default     = ""
}
variable "argocd_ssh_key_create" {
  description = "Create SSH key for ArgoCD repository access"
  type        = bool
  default     = false
}
variable "argocd_ssh_key_repositories" {
  description = "List of repositories to be added to ArgoCD"
  type        = list(string)
  default     = []
  validation {
    condition = alltrue([for repo in var.argocd_ssh_key_repositories : can(regex("git@.*", repo))])
    error_message = "Only SSH git repositories are supported."
  }
}
variable "istio_create" {
  description = "Create Istio deployment"
  type        = bool
  default     = false
}
variable "prom_stack_create" {
  default     = false
  type        = bool
  description = "Create *Kubernetes Prometheus Stack* release"
}
variable "prom_stack_grafana_host" {
  description = "Hostname for Grafana"
  type        = string
  default     = ""
}

## Application Settings
variable "app_env_vars" {
  type        = map(map(string))
  default     = {}
  description = "Postgres environment variables"
}
