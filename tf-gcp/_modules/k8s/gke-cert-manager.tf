###* Cert-Manager *###

resource "kubernetes_namespace" "cert_manager" {
  count = var.cert_manager_create ? 1 : 0
  metadata {
    name   = "cert-manager"
    labels = { managed_by = "terraform" }
  }
}

resource "helm_release" "cert_manager" {
  count      = var.cert_manager_create ? 1 : 0
  depends_on = [kubernetes_namespace.cert_manager]

  name       = "cert-manager"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  namespace  = "cert-manager"
  version    = "v1.14.3"
  timeout    = "300"
  values = [
    <<-EOF
installCRDs: true
# serviceAccount:
#   annotations:
#     iam.gke.io/gcp-service-account: "sa-dns-solver@${var.project_id}.iam.gserviceaccount.com"

EOF
  ]
}

locals {
  issuer_ingress_class_nginx = var.ingress_create ? {
    "nginx" = {
      issuer = "letsencrypt-http"
      ingress_class = "nginx"
    }
  } : {}
  issuer_ingress_class_istio = var.istio_create ? {
    "istio" = {
      issuer = "letsencrypt-http-istio"
      ingress_class = "istio"
    }
  } : {}
  issuer_ingress_classes = merge(local.issuer_ingress_class_istio, local.issuer_ingress_class_nginx)
}

resource "kubernetes_manifest" "cert_manager_http_issuer" {
  for_each   = var.cert_manager_issuer_create ? local.issuer_ingress_classes : {}
  depends_on = [kubernetes_namespace.cert_manager, helm_release.cert_manager]

  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "ClusterIssuer"
    metadata = {
      name   = each.value.issuer
      labels = { managed_by = "terraform" }
    }
    spec = {
      acme = {
        server = "https://acme-v02.api.letsencrypt.org/directory"
        email  = var.admin_email
        privateKeySecretRef = {
          name = each.value.issuer
        }
        solvers = [
          # HTTP
          {
            selector = {}
            http01 = {
              ingress = {
                # NOTE: If you want to use another ingress-class to generate a certificate via acme-challenge:
                # NOTE: add the following annotation to Ingress resource: `acme.cert-manager.io/http01-ingress-class=YOUR_INGRESS_CLASS_NAME`
                class = each.value.ingress_class
              }
            }
          },
        ]
      }
    }
  }
}

/*
#? In case you want to use DNS solver with json key
resource "kubernetes_secret" "cert_manager_sa_dns_solver_creds_0" {
  count      = 0
  depends_on = [kubernetes_namespace.cert_manager]
  metadata {
    name      = "cert-manager-sa-dns-solver-creds"
    namespace = "cert-manager"
    labels    = { managed_by = "terraform" }
  }

  data = {
    secret-access-key = jsondecode(data.terraform_remote_state.gcp.outputs.sa_dns_solver_creds)
  }
  type = "Opaque"
}

resource "kubernetes_manifest" "cert_manager_dns_issuer" {
  count      = var.cert_manager_issuer_create ? 1 : 0
  depends_on = [kubernetes_namespace.cert_manager, helm_release.cert_manager]

  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "ClusterIssuer"
    metadata = {
      name   = "letsencrypt-dns"
      labels = { managed_by = "terraform" }
    }
    spec = {
      acme = {
        server = "https://acme-v02.api.letsencrypt.org/directory"
        email  = "admin@${var.base_domain}"
        privateKeySecretRef = {
          name = "letsencrypt-dns"
        }
        solvers = [
          #? Using SA Workload Identity
          {
            selector = {}
            # selector = { dnsZones = [var.domain_default] }
            dns01 = {
              cloudDNS = {
                project = var.project_id
              }
            }
          },

          #? Using SA Key
          # {
          #   # selector = { dnsZones = [var.domain_default] }
          #   dns01 = {
          #     cloudDNS = {
          #       project = var.project_id
          #       serviceAccountSecretRef = {
          #         name = "cert-manager-sa-dns-solver-creds"
          #         key = "secret-access-key"
          #       }
          #     }
          #   }
          # },
        ]
      }
    }
  }
}

resource "kubernetes_manifest" "cert_wildcard" {
  count      = var.cert_manager_issuer_create ? 1 : 0
  depends_on = [kubernetes_manifest.cert_manager_dns_issuer]

  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "Certificate"
    metadata = {
      name      = "wildcard.${var.base_domain}-tls"
      namespace = var.gke_namespace_main
      labels    = { managed_by = "terraform" }
    }
    spec = {

      secretTemplate = {
        annotations = {
          "reflector.v1.k8s.emberstack.com/reflection-allowed" = "true"
          "reflector.v1.k8s.emberstack.com/reflection-allowed-namespaces" = "review-.*"
          "reflector.v1.k8s.emberstack.com/reflection-auto-enabled" = "true"
          "reflector.v1.k8s.emberstack.com/reflection-auto-namespaces" = "review-.*"
        }
      }
      dnsNames = [
        "*.${var.base_domain}",
      ]
      issuerRef = {
        group = "cert-manager.io"
        kind  = "ClusterIssuer"
        name  = "letsencrypt-dns"
      }
      secretName = "wildcard.${var.base_domain}-tls"
    }
  }
}


### Reflect changes to mirror resources in the same or other namespaces.
resource "helm_release" "cert_reflector" {
  depends_on = [kubernetes_namespace.cert_manager]
  name       = "cert-reflector"
  repository = "https://emberstack.github.io/helm-charts"
  chart      = "reflector"
  namespace  = "cert-manager"
  version    = "6.1.23"
  timeout    = "300"
  values = [
    <<-EOF
configuration:
  watcher:
    timeout: 600 # Workaround for https://github.com/emberstack/kubernetes-reflector/issues/246
EOF
  ]
}
*/
