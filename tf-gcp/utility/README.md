# utility project

## APIs for utility project

If you want to use SA from a utility project to manage other projects,
you need to have some Google APIs enabled in utility project.  

Minimum required APIs are:
- Cloud Resource Manager API
- Cloud IAM API
