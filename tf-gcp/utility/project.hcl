## This GCP project is used for utility purposes to handle common resources (eg: Shared VPC, DNS, etc)

locals {
  project_id     = "voxmaster"
  region_default = "europe-west1"
  zone_default   = "europe-west1-b"
}
