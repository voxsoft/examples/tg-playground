resource "google_dns_managed_zone" "voxsoft_pro" {
  project     = var.project_id
  name        = "voxsoft-pro"
  dns_name    = "voxsoft.pro."
  description = "DNS zone where everything begins..."
  labels = {
    "managed_by" = "terraform"
    "level"      = "root"
  }
}

output "voxsoft_pro_ns" {
  value = google_dns_managed_zone.voxsoft_pro.name_servers
}



### DNS Records
## TODO: Move to a separate module applicable for other projects
resource "google_dns_record_set" "beerit_voxsoft_pro_ingress_ip0" {
  for_each     = toset(["beerit-api", "beerit", "beerit-admin", "gow", "gow-ws", "argocd", "grafana"])
  project      = var.project_id
  name         = "${each.key}.voxsoft.pro."
  type         = "A"
  ttl          = 6000
  managed_zone = google_dns_managed_zone.voxsoft_pro.name
  rrdatas      = ["34.140.130.3"]
}
resource "google_dns_record_set" "beerit_voxsoft_pro_ingress_ip1" {
  for_each     = toset(["istio0", "istio1", "istio2"])
  project      = var.project_id
  name         = "${each.key}.voxsoft.pro."
  type         = "A"
  ttl          = 6000
  managed_zone = google_dns_managed_zone.voxsoft_pro.name
  rrdatas      = ["34.140.227.244"]
}
