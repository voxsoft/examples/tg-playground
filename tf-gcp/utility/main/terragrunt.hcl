## Terraform Module
# Local

## Inlcudes
include "root" { # Root level config
  path = find_in_parent_folders("root.hcl")
}
include "gcp" { # GCP level config
  path = find_in_parent_folders("gcp.hcl")
}
generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "google" { project = "${local.project.project_id}" }
EOF
}

## Locals and inputs
locals {
  project = read_terragrunt_config(find_in_parent_folders("project.hcl")).locals # Import project locals
}
inputs = {
  project_id = local.project.project_id
  region_default = local.project.region_default
}
