
module "sa_terraform" {
  source      = "../../_modules/service-account"
  name        = "sa-terraform"
  project_id  = var.project_id
  description = "Service Account Managed and Used by Terraform"

  key_create        = true
  key_rotation_days = 90

  iam_roles = [
    "roles/iam.serviceAccountAdmin",
    "roles/iam.serviceAccountKeyAdmin",
    "roles/dns.admin",
  ]
}

output "sa_terraform_key_b64" {
  value     = module.sa_terraform.key_b64
  sensitive = true
}
output "sa_terraform_email" {
  value = module.sa_terraform.email
}
