module "gcs_bucket_games_backups" {
  source        = "terraform-google-modules/cloud-storage/google//modules/simple_bucket"
  version       = "6.1.0"
  name          = "voxmaster-games-backups"
  project_id    = var.project_id
  force_destroy = false
  location      = var.region_default
  versioning    = false
  
  lifecycle_rules = [
    {
      ## Delete object versions older than 1 day
      action    = { type = "Delete" }
      condition = { matches_prefix = "Valheim/,MC/", days_since_noncurrent_time = 1 }
    }
  ]
}

module "sa_games_backups" {
  source      = "../../_modules/service-account"
  name        = "sa-games-backups"
  project_id  = var.project_id
  description = "Service Account Managed and Used by Terraform"

  key_create        = true
  key_rotation_days = 365

  iam_roles = [
    "roles/storage.objectUser",
  ]
}

output "sa_games_backups_b64" {
  value     = module.sa_games_backups.key_b64
  sensitive = true
}
