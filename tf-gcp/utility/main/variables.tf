variable "project_id" {
  type        = string
  description = "GCP project id"
}
variable "region_default" {
  type        = string
  description = "Default GCP region"
}
