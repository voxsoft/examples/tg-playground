variable "project_id" {
  description = "GCP Project ID"
}
variable "name" {
  type        = string
  description = "Service Account Name to create"
}
variable "display_name" {
  type        = string
  description = "Service Account Display Name"
  default     = null
}
variable "description" {
  type        = string
  description = "Service Account Description"
  default     = "Service Account Managed by Terraform"
}
variable "json_key_create" {
  type        = bool
  description = "Create Service Account JSON Key"
  default     = false
}
variable "iam_roles" {
  type        = list(string)
  description = "List of roles for IAM binding"
  default     = null
}

variable "wi_enable" {
  type        = bool
  description = "Create Workload Identity for this Service Account"
  default     = false
}
variable "wi_kubernetes_sa_path" {
  type        = string
  description = "Kubernetes Service Account Path for Workload Identity. [NAMESPACE/SA_NAME]"
  default     = "a/a"
  validation {
    condition     = can(regex(".*/.*", var.wi_kubernetes_sa_path))
    error_message = "The `wi_kubernetes_sa_path` value must be in \"NAMESPACE/SA_NAME\" format."
  }
}
