resource "google_service_account" "this" {
  account_id   = var.name
  display_name = var.display_name
  description  = var.description
}

resource "google_project_iam_member" "this" {
  for_each = toset(var.iam_roles)
  project  = var.project_id
  role     = each.key
  member   = "serviceAccount:${google_service_account.this.email}"
}

# Workload Identity for SA. ReadMore: https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity
resource "google_service_account_iam_member" "this_wi" {
  count              = var.wi_enable ? 1 : 0
  service_account_id = google_service_account.this.name
  role               = "roles/iam.workloadIdentityUser"
  member             = "serviceAccount:${var.project_id}.svc.id.goog[${var.wi_kubernetes_sa_path}]"
}
resource "google_service_account_key" "this" {
  count              = var.json_key_create ? 1 : 0
  service_account_id = google_service_account.this.name
}

output "json_key_b64" {
  value       = var.json_key_create ? google_service_account_key.this[0].private_key : null
  sensitive   = true
  description = "JSON Key of the Service Account, base64 encoded"
}

output "email" {
  value       = google_service_account.this.email
  description = "Service Account Email"
}

# TODO: Add features for SA Key, like key storing in Secret Manager
# You can store it in Google Secret Manager
/*
resource "google_secret_manager_secret" "this" {
  count     = 0
  secret_id = "this"
  replication {
    user_managed {
      replicas { location = var.gcp_region_default }
    }
  }
  labels = { managed_by = "terraform" }
}
resource "google_secret_manager_secret_version" "this" {
  count      = 0
  depends_on = [google_secret_manager_secret.this]

  secret      = google_secret_manager_secret.this[0].id
  secret_data = google_service_account_key.this[0].private_key
}
output "this_json_b64" {
  value     = true ? google_service_account_key.this[0].private_key : null
  sensitive = true
}
*/
