## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | 4.0.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_project_iam_member.this](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_service_account.this](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |
| [google_service_account_iam_member.this_wi](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account_iam_member) | resource |
| [google_service_account_key.this](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account_key) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_description"></a> [description](#input\_description) | Service Account Description | `string` | `"Service Account Managed by Terraform"` | no |
| <a name="input_display_name"></a> [display\_name](#input\_display\_name) | Service Account Display Name | `string` | `null` | no |
| <a name="input_iam_roles"></a> [iam\_roles](#input\_iam\_roles) | List of roles for IAM binding | `list(string)` | `null` | no |
| <a name="input_json_key_create"></a> [json\_key\_create](#input\_json\_key\_create) | Create Service Account JSON Key | `bool` | `false` | no |
| <a name="input_name"></a> [name](#input\_name) | Service Account Name to create | `string` | n/a | yes |
| <a name="input_wi_enable"></a> [wi\_enable](#input\_wi\_enable) | Create Workload Identity for this Service Account | `bool` | `false` | no |
| <a name="input_wi_kubernetes_sa_path"></a> [wi\_kubernetes\_sa\_path](#input\_wi\_kubernetes\_sa\_path) | Kubernetes Service Account Path for Workload Identity. [NAMESPACE/SA\_NAME] | `string` | `"a/a"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_email"></a> [email](#output\_email) | Service Account Email |
| <a name="output_json_key_b64"></a> [json\_key\_b64](#output\_json\_key\_b64) | JSON Key of the Service Account, base64 encoded |
