## Terraform Module
# local

## Inlcudes
include "root" { # Root level config
  path = find_in_parent_folders("root.hcl")
}
include "gcp" { # GCP level config
  path = find_in_parent_folders("gcp.hcl")
}
generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "google" { project = "${local.project.project_id}" }
provider "google-beta" { project = "${local.project.project_id}" }
EOF
}

## Dependenies, locals and inputs
dependency "gcp_utility_sa" {
  config_path = "${get_path_to_repo_root()}/tf-gcp/utility/main"
  mock_outputs = {
    sa_terraform_email = null
  }
}

## Locals and inputs
locals {
  project = read_terragrunt_config(find_in_parent_folders("project.hcl")).locals # Import project locals
}
inputs = {
  project_id         = local.project.project_id
  terraform_sa_email = dependency.gcp_utility_sa.outputs.sa_terraform_email
}
