variable "googleapis" {
  description = "List of GCP APIs to enable"
  type        = list(string)
  default     = []
/*
  default = [ # A list of Google APIs to be enabled, default is fine. Ones applied, can be set to [] 
    "compute.googleapis.com",
    "sql-component.googleapis.com",
    "sqladmin.googleapis.com",
    "servicenetworking.googleapis.com",
    // "redis.googleapis.com",
    "vpcaccess.googleapis.com",
    "stackdriver.googleapis.com",
    "logging.googleapis.com",
    "iamcredentials.googleapis.com",
    "cloudbuild.googleapis.com",
    "cloudfunctions.googleapis.com",
    "cloudscheduler.googleapis.com",
    "eventarc.googleapis.com",
    "firebase.googleapis.com",
    "iap.googleapis.com",
    // "sourcerepo.googleapis.com",
    // "secretmanager.googleapis.com",
    "dns.googleapis.com",
    "domains.googleapis.com",
    "bigquery.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "monitoring.googleapis.com",
    "container.googleapis.com",
    "iam.googleapis.com",
    "storage-component.googleapis.com",
    "recommender.googleapis.com",
    "serviceusage.googleapis.com",
    "run.googleapis.com",
    "pubsub.googleapis.com",
    "cloudkms.googleapis.com",
    "translate.googleapis.com",
    // "servicemanagement.googleapis.com",
    // "servicecontrol.googleapis.com",
    // "endpoints.googleapis.com",
  ]
*/
}

resource "google_project_service" "googleapis" {
  for_each           = toset(var.googleapis)
  project            = var.project_id
  service            = each.key
  disable_on_destroy = "false"
}
