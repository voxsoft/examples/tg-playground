variable "project_id" {
  type        = string
  description = "GCP project id"
}
variable "terraform_sa_email" {
  type        = string
  default     = null
  description = "Service Account email for Terraform"
}
