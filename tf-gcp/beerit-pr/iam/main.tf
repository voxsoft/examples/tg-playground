###* Applications SA | IAM Access *###

#* Terraform SA *#
resource "google_project_iam_member" "sa_terraform" {
  count   = var.terraform_sa_email != null ? 1 : 0
  project = var.project_id
  role    = "roles/owner"
  member  = "serviceAccount:${var.terraform_sa_email}"
}

#* Core API Runner *#
# This SA is used to run Core API with permission described in IAM
module "sa_coreapi" {
  source     = "./modules/service-account"
  project_id = var.project_id

  name                  = "sa-coreapi"
  description           = "Service Account for Core API. Managed by Terraform"
  wi_enable             = true
  wi_kubernetes_sa_path = "main/coreapi"
  json_key_create       = false
  iam_roles = [
    "roles/cloudsql.client",
    # "roles/servicemanagement.serviceController",
    "roles/storage.objectAdmin",
    "roles/cloudtranslate.user", # GOW Next
    # "roles/iam.serviceAccountTokenCreator", # Signed URLs
    # "roles/pubsub.subscriber",
    # "roles/pubsub.publisher",
    # "roles/cloudfunctions.invoker",
    # "roles/bigquery.user",
    # "roles/cloudiot.viewer",
    # "roles/datastore.user",
    # "roles/firebaseauth.admin",
    # "roles/firebaseauth.viewer",
  ]
}

output "sa_coreapi" {
  value = {
    email        = module.sa_coreapi.email
    json_key_b64 = module.sa_coreapi.json_key_b64
  }
  sensitive = true
}

#* ArgoCD *#
module "sa_argocd" {
  source     = "./modules/service-account"
  project_id = var.project_id

  name                  = "sa-argocd-repo-server"
  description           = "Service Account for ArgoCD. Managed by Terraform"
  wi_enable             = true
  wi_kubernetes_sa_path = "argocd/argocd-repo-server"
  json_key_create       = false
  iam_roles             = [ "roles/cloudkms.cryptoKeyDecrypter" ]
}
