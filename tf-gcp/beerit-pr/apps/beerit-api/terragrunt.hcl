## Terraform Module
terraform {
  source = "${get_path_to_repo_root()}/tf-gcp/_modules//app"
}

## Inlcudes
include "root" { # Root level config
  path = find_in_parent_folders("root.hcl")
}
generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "google" { project = "${local.project.project_id}" }
provider "google-beta" { project = "${local.project.project_id}" }
EOF
}

## Locals and inputs
dependency "main" {
  config_path = "../../main"
  mock_outputs = {
    network_name      = "default"
    network_self_link = null
  }
}
locals {
  project  = read_terragrunt_config(find_in_parent_folders("project.hcl")).locals # Import project locals
  app_name = "beerit-api"
}
inputs = {
  project_id           = local.project.project_id
  region_default       = local.project.region_default
  zone_default         = local.project.zone_default
  resource_name_prefix = local.project.resource_name_prefix
  deletion_protection  = true

  app_name = local.app_name
  postgres_instances = {
    "${local.app_name}" = {
      db_name   = "beerit"
      user_name = "beerit"
      ip_configuration = {
        private_network = dependency.main.outputs.network_self_link
      }
      additional_databases = [
        {
          name      = "gow"
          charset   = "UTF8"
          collation = "en_US.UTF8"
        },
      ]
      additional_users = [
        {
          name            = "gow"
          password        = null
          random_password = true
        }
      ]
      export_to_gcs = {
        bucket_name = "beerit-pr-backups" ## Bucket name to export the database
        databases   = ["gow"]             ## List of databases to export
        schedule    = "0 1 * * 0"         ## Schedule for the export
      }
    }
  }
  gcs_buckets = {
    "beerit-pr-images" = {
      location       = local.project.region_default
      uniform_access = false
      // iam_sa_add = true ## Add service account as storage.objectAdmin for the bucket (default:true)
      // iam_members = [{
      //   role   = "roles/storage.admin"
      //   member = "user:someuser@gmail.com"
      // }]
    }
  }
  iam_sa = {
    create = true
    name   = "${local.app_name}"
    roles = [
      "roles/cloudsql.client"
    ]
    workload_identity_paths = ["main/${local.app_name}"]
  }
}
