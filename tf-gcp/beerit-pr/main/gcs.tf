module "gcs_bucket_backups" {
  source        = "terraform-google-modules/cloud-storage/google//modules/simple_bucket"
  version       = "4.0.0"
  name          = "beerit-pr-backups"
  project_id    = var.project_id
  force_destroy = false
  location      = var.region_default
  versioning    = false
}
