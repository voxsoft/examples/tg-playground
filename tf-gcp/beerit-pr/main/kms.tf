module "kms_sops" {
  source  = "terraform-google-modules/kms/google"
  version = "~> 2.3"

  project_id         = var.project_id
  location           = var.region_default
  keyring            = "sops-keyring"
  keys               = ["argocd"]
}
