output "network_self_link" {
  value = module.network.network_self_link
  description = "The self link of the network"
}
