module "gke" {
  count = 1
  depends_on = [ module.network ]
  # source                     = "terraform-google-modules/kubernetes-engine/google"
  source                     = "terraform-google-modules/kubernetes-engine/google//modules/private-cluster"
  version                    = "~> 27.0.0"
  project_id                 = var.project_id
  name                       = var.gke_cluster_name
  regional                   = false
  region                     = var.region_default
  zones                      = [var.zone_default]
  network                    = module.network.network_name
  subnetwork                 = "${var.network_name}-${var.region_default}-gke"
  ip_range_pods              = "${var.network_name}-${var.region_default}-gke-pods"
  ip_range_services          = "${var.network_name}-${var.region_default}-gke-services"
  grant_registry_access      = true
  http_load_balancing        = true
  network_policy             = false
  horizontal_pod_autoscaling = true
  filestore_csi_driver       = false
  release_channel            = "REGULAR"
  maintenance_start_time     = "2020-01-19T04:00:00Z"
  maintenance_end_time       = "2020-01-19T09:00:00Z"
  maintenance_recurrence     = "FREQ=WEEKLY;BYDAY=SU,FR,SA"
  master_authorized_networks = [{ cidr_block = "0.0.0.0/0", display_name = "all" }]
  logging_service            = "none" # Reduce project costs for testing
  monitoring_service         = "none" # Reduce project costs for testing
  ### Private Cluster
  enable_private_nodes       = true
  enable_private_endpoint    = false
  master_ipv4_cidr_block     = "10.132.127.0/28"
  ### Firewall
  add_master_webhook_firewall_rules = true

  node_pools = [
    {
      name                      = "default-node-pool"
      machine_type              = "e2-standard-2"
      spot                      = true
      min_count                 = 1
      max_count                 = 2
      disk_size_gb              = 30
      disk_type                 = "pd-standard"
      image_type                = "COS_CONTAINERD"
      auto_repair               = true
      auto_upgrade              = true
    },
  ]
  node_pools_labels = {
    all = {
      workload-type = "application"
      workload-env  = var.env_name
    }
  }
  node_pools_metadata = {
    all = {
      disable-legacy-endpoints = "true"
      block-project-ssh-keys   = "true"
    }
  }

}

###* GKE External Ingress IP *###
resource "google_compute_address" "gke_ingress" {
  count        = var.ingress_ips_num
  provider     = google-beta
  project      = var.project_id
  region       = var.region_default
  name         = "${var.gke_cluster_name}-ing-ip${count.index}"
  address_type = "EXTERNAL"
  network_tier = "PREMIUM"

  labels      = { managed_by = "terraform" }
  description = "Ingress Static IP address for GKE Cluster. Managed by Terraform."
}
output "gke_ingress_ips" {
  value = var.ingress_ips_num > 0 ? google_compute_address.gke_ingress[*].address : []
}
