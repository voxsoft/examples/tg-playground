variable "env_name" {
  type        = string
  description = "Environment name"
}
variable "project_id" {
  type        = string
  description = "GCP project id"
}
variable "region_default" {
  type        = string
  description = "Default GCP region"
}
variable "zone_default" {
  type        = string
  description = "Default GCP zone"
}
variable "network_name" {
  type        = string
  description = "VPC network name"
}
variable "ingress_ips_num" {
  type = number
  description = "Number of IPs reserved for ingress"
  default = 1
}
variable "gke_cluster_name" {
  type        = string
  description = "GKE cluster name"
}
