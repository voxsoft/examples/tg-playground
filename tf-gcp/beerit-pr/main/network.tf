module "network" {
  source = "../../_modules/network"
  
  project_id   = var.project_id
  network_name = var.network_name
  subnets = [
    {
      subnet_name           = "${var.network_name}-${var.region_default}-gke"
      subnet_ip             = "10.132.80.0/20"
      subnet_private_access = "true"
      subnet_region         = var.region_default
      description           = "GKE subnet"
    },
    {
      subnet_name           = "${var.network_name}-${var.region_default}-vm"
      subnet_ip             = "10.132.128.0/24"
      subnet_private_access = "true"
      subnet_region         = var.region_default
      description           = "Standalone VMs subnet"
    },
  ]
  secondary_ranges = {
    "${var.network_name}-${var.region_default}-gke" = [
      {
        range_name    = "${var.network_name}-${var.region_default}-gke-pods"
        ip_cidr_range = "10.132.0.0/18"
      },
      {
        range_name    = "${var.network_name}-${var.region_default}-gke-services"
        ip_cidr_range = "10.132.64.0/20"
      },
    ]
  }
  firewall_rules = [{
    name        = "${var.network_name}-allow-iap"
    direction   = "INGRESS"
    description = "Allow connection from Identity-Aware Proxy. Managed by Terraform."
    ranges      = ["35.235.240.0/20"]
    allow = [{
      protocol = "tcp"
      ports    = ["22", "3389"]
    }]
    # deny                    = []
    # priority                = null
    # source_tags             = null
    # source_service_accounts = null
    # target_tags             = ["ssh", "rdp"]
    # target_service_accounts = null
    # log_config              = null
  }]
  nat_enabled = true
  ## Debug
  spin_up_test_vm = false
}
