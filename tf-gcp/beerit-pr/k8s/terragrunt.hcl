## Terraform Module
terraform {
  source = "${get_path_to_repo_root()}/tf-gcp/_modules//k8s"
}

## Inlcudes
include "root" { # Root level config
  path = find_in_parent_folders("root.hcl")
}
include "gcp" { # GCP level config
  path = find_in_parent_folders("gcp.hcl")
}
generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "google" { project = "${local.project.project_id}" }
provider "google-beta" { project = "${local.project.project_id}" }
EOF
}

## Dependenies, locals and inputs
dependency "main" {
  config_path = "../main"
  mock_outputs = {
    gke_ingress_ips = []
  }
}
dependency "apps_beerit_api" {
  config_path = "../apps/beerit-api"
  mock_outputs = {
    app_env_vars = {}
  }
}
dependency "gitlab_agent" {
  config_path = "${get_path_to_repo_root()}/tf-gitlab/agents"
  mock_outputs = {
    gitlab_cluster_agent_token = ""
  }
}
locals {
  project = read_terragrunt_config(find_in_parent_folders("project.hcl")).locals # Import project locals
  gcp     = read_terragrunt_config(find_in_parent_folders("gcp.hcl")).locals     # Import project locals
}
inputs = {
  project_id = local.project.project_id
  ## TODO: Get the following values from gke module outputs
  region_default       = local.project.region_default
  zone_default         = local.project.zone_default
  gke_cluster_regional = local.project.resources_ha

  gke_cluster_name = local.project.gke_cluster_name

  ## Ingress
  ingress_create = true
  ingress_ips    = dependency.main.outputs.gke_ingress_ips

  ## Cert Manager
  cert_manager_create        = true
  cert_manager_issuer_create = true ## Must be `false` on a first run
  admin_email                = local.gcp.admin_email

  ## Gitlab Agent
  gitlab_agent_create = true
  gitlab_agent_token  = dependency.gitlab_agent.outputs.gitlab_cluster_agent_token

  ## Apps
  app_env_vars = merge(dependency.apps_beerit_api.outputs.app_env_vars) # Here we merge all the env vars from all the apps

  ## ARGO CD
  argocd_create               = true
  argocd_host                 = "argocd.voxsoft.pro"
  argocd_ssh_key_create       = true
  argocd_ssh_key_repositories = ["git@gitlab.com:voxsoft/beerit/beerit-ci.git"]

  ## Istio
  istio_create = false

  ## Metrics
  prom_stack_create = false
  prom_stack_grafana_host = "grafana.voxsoft.pro"
}
