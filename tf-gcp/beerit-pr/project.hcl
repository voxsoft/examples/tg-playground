## This GCP Project will host BeerIT project Infrastructure
## This is a primer environment. We do not know yet if we will split it into other environments

locals {
  env_name       = "pr"
  project_id     = "beerit-pr"
  region_default = "europe-west1"
  zone_default   = "europe-west1-b"

  resource_name_prefix = "beerit-pr"
  resources_ha         = false
  deletion_protection  = false

  network_name         = "beerit-pr"
  gke_cluster_name     = "beerit-pr"
}
