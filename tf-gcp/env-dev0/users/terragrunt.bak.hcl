include "root" {
  path = find_in_parent_folders()
}
dependencies {
  paths = ["../apis"]
}
terraform {
  source = "${get_parent_terragrunt_dir()}/_modules//users"
}

locals {
  project_vars = read_terragrunt_config(find_in_parent_folders("project.hcl"))
}
generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "google" {
  project     = "${local.project_vars.locals.gcp_project_id}"
  region      = "${local.project_vars.locals.gcp_region_default}"
}
EOF
}


inputs = {
  project_id = local.project_vars.locals.gcp_project_id
  iam_users = {
    ## NOTE: dots in gmail account can produce errors
    "oleksyy.marchenko@gmail.com" = [
      "roles/editor",
    ],
  }
}
