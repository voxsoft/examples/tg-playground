module "sa_coreapi" {
  source     = "../../_modules/service-account"
  project_id = var.project_id

  name        = "sa-vm-mig-example"
  description = "Service Account for MIG. Managed by Terraform"
  iam_roles = [
    "roles/cloudsql.client",
    "roles/storage.objectAdmin",
    "roles/artifactregistry.reader",
  ]
}

module "instance_template" {
  source     = "terraform-google-modules/vm/google//modules/instance_template"
  version    = "~> 8.0.1"
  project_id = var.project_id
  # network            = var.network_name
  subnetwork         = var.subnetwork
  subnetwork_project = var.project_id
  service_account = {
    email  = module.sa_coreapi.email
    scopes = ["cloud-platform"]
  }
  machine_type = "e2-micro"
  name_prefix  = "vm-mig-example-${var.resource_name_prefix}-${var.app_name}"
  disk_size_gb = 15
  ## Regular VM 
  /*
  source_image_project = "ubuntu-os-cloud"
  source_image_family = "ubuntu-2204-lts"
  startup_script = <<-EOF
    #!/bin/bash
    set -e
    apt-get update -y
    apt-get install -y nginx
    service nginx start
  EOF 
  */
  ## Automatically run Container image in COS VM
  source_image_project = "cos-cloud"
  source_image_family  = "cos-stable"
  metadata = {
    gce-container-declaration = <<EOT
spec:
  containers:
    - image: nginx:stable
      name: containervm
      securityContext:
        privileged: false
      env:
        - name: foo
          value: bar
      stdin: false
      tty: false
      volumeMounts: []
      restartPolicy: Always
      volumes: []
EOT
  }
  tags = [ "http-server", "https-server" ]
}

module "mig" {
  source            = "terraform-google-modules/vm/google//modules/mig"
  version           = "~> 8.0.1"
  project_id        = var.project_id
  region            = var.region_default
  target_size       = var.target_size
  hostname          = "vm-mig-example0"
  instance_template = module.instance_template.self_link
  named_ports = [
    {
      name = "http"
      port = 80
    }
  ]

  # distribution_policy_zones = [var.zone_default]
}

module "lb_http" {
  source            = "terraform-google-modules/lb-http/google"
  version           = "~> 9.1.0"
  project           = var.project_id
  name              = "lb-vm-mig-example"
  firewall_networks = [var.network]

  ssl                  = var.tls_use_managed_certs ? true : false
  https_redirect       = var.tls_use_managed_certs ? true : false
  use_ssl_certificates = false
  managed_ssl_certificate_domains = var.tls_use_managed_certs ? var.tls_managed_certs_domains : []

  backends = {
    default = {
      protocol    = "HTTP"
      port        = 80
      port_name   = "http"
      timeout_sec = 10
      enable_cdn  = false

      health_check = {
        request_path = "/"
        port         = 80
      }

      log_config = {
        enable = false
      }

      groups = [
        {
          group = module.mig.instance_group
        }
      ]
      iap_config = { enable = false }
    }
  }
}

output "lb_ip" {
  value = module.lb_http.external_ip
}
