variable "project_id" {
  type        = string
  description = "GCP project id"
}
variable "region_default" {
  type        = string
  description = "Default GCP region"
}
variable "zone_default" {
  type        = string
  description = "Default GCP zone"
}
variable "network" {
  type        = string
  description = "VPC network name"
  default     = "default"
}

variable "subnetwork" {
  description = "The subnetwork to host the compute instances in"
}
variable "resource_name_prefix" {
  type        = string
  description = "Prefix for resource names"
}
variable "target_size" {
  description = "The target number of running instances for this managed instance group. This value should always be explicitly set unless this resource is attached to an autoscaler, in which case it should never be set."
  default = 2
}
variable "app_name" {
  type        = string
  description = "Application name"
}
variable "tls_use_managed_certs" {
  type        = bool
  description = "Use managed certificates for TLS termination on LB"
  default     = true
}
variable "tls_managed_certs_domains" {
  type        = list
  description = "Managed certificates domains"
  default     = ["beerit-api.del14.voxsoft.pro"]
}
