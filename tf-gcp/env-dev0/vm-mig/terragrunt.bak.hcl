## Terraform Module
# Local

## Inlcudes
include "root" { # Root level config
  path = find_in_parent_folders("root.hcl")
}
generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "google" { project = "${local.project.project_id}" }
provider "google-beta" { project = "${local.project.project_id}" }
EOF
}

## Locals and inputs
dependency "main" {
  config_path = "../main"
  mock_outputs = {
    network_name      = "default"
    network_self_link = null
  }
}
locals {
  project  = read_terragrunt_config(find_in_parent_folders("project.hcl")).locals # Import project locals
  app_name = "beerit-api"
}
inputs = {
  project_id           = local.project.project_id
  region_default       = local.project.region_default
  zone_default         = local.project.zone_default
  resource_name_prefix = local.project.resource_name_prefix
  deletion_protection  = false

  app_name   = local.app_name
  subnetwork = dependency.main.outputs.subnets.vm
  network    = dependency.main.outputs.network_self_link
}
