# ! DO NOT PLACE SECURITY-SENSITIVE VALUES HERE ! #
# Read more in README.md

locals {
  env_name       = "env-dev0"
  project_id     = "deleteme14"
  region_default = "us-central1"
  zone_default   = "us-central1-a"

  resource_name_prefix = "del14"
  network_name         = local.resource_name_prefix
}
