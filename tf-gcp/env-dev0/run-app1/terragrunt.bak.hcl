include "root" {
  path = find_in_parent_folders()
}

terraform {
  source = "${get_parent_terragrunt_dir()}/_modules//run-app"
}

locals {
  project_vars = read_terragrunt_config(find_in_parent_folders("project.hcl"))
}
generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "google" {
  project     = "${local.project_vars.locals.gcp_project_id}"
  region      = "${local.project_vars.locals.gcp_region_default}"
}
provider "google-beta" {
  project     = "${local.project_vars.locals.gcp_project_id}"
  region      = "${local.project_vars.locals.gcp_region_default}"
}
EOF
}


inputs = {
  project_id        = local.project_vars.locals.gcp_project_id
  service_name      = "app1"
  location          = "${local.project_vars.locals.gcp_region_default}"
  image             = "us-central1-docker.pkg.dev/deleteme-project-328717/gcsproxy/test:1"
  container_command = ["/gcsproxy"]
  argument          = ["-b", "0.0.0.0:8080"]
  # members                = ["allUsers"]
  iap_config = { ## Non Organization projects must use GCP Console to generate OAuth client and secrets
    enable = true
    ##! WARNING: This is Security Risk. Use only for testing
    oauth2_client_id     = "123456787628-r4hjhahar91anpffgs6k6v12urjs98kn.apps.googleusercontent.com"
    oauth2_client_secret = "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"
    # iam_allowed_users    = ["user:oleksyy.marchenko@gmail.com"] ## Who can access the service via IAP
    ## NOTE: To activate IAP You should go to GCP Console, disable and enable IAP for this service
  }
  managed_ssl_certificate_domains = ["run-app1.voxsoft.pro"]
}
