include "root" {
  path = find_in_parent_folders()
}
dependencies {
  paths = ["../vpc"]
}
dependency "vpc" {
  config_path = "../vpc"
}
terraform {
  source = "${get_parent_terragrunt_dir()}/_modules//vault"
}

generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "google" {
  project     = "${local.project_vars.locals.gcp_project_id}"
  region      = "${local.project_vars.locals.gcp_region_default}"
}
provider "google-beta" {
  project     = "${local.project_vars.locals.gcp_project_id}"
  region      = "${local.project_vars.locals.gcp_region_default}"
}
EOF
}

locals {
  project_vars = read_terragrunt_config(find_in_parent_folders("project.hcl"))
}
inputs = {
  env_name   = local.project_vars.locals.env_name
  project_id = local.project_vars.locals.gcp_project_id
  region     = local.project_vars.locals.gcp_region_default
  zone       = local.project_vars.locals.gcp_zone_default

  network = dependency.vpc.outputs.network_name
  subnet  = dependency.vpc.outputs.subnets_names[0]

  ## Vault
  allow_public_egress = false
  tls_cn              = "vault.${local.project_vars.locals.base_domain}"
}

