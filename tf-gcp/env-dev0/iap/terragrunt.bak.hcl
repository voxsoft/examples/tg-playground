include "root" {
  path = find_in_parent_folders()
}

terraform {
  source = "${get_parent_terragrunt_dir()}/_modules//iap"
}

locals {
  project_vars = read_terragrunt_config(find_in_parent_folders("project.hcl"))
}
generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "google" {
  project     = "${local.project_vars.locals.gcp_project_id}"
  region      = "${local.project_vars.locals.gcp_region_default}"
}
EOF
}


inputs = {
  project = local.project_vars.locals.gcp_project_id

  iap_brand_create = false ## NOTE: IAP brand creation working only for organizations. For private projects brand should be created via GCP Console
  iap_brand_name   = local.project_vars.locals.gcp_project_id
  # iap_clients = [ "IAP-WebApps" ] ## NOTE: This option working only for organizations. For private projects OAuth should be created via GCP Console
  support_email = "user:oleksi.marchenko@gmail.com"
  allowed_users = [
    "user:oleksi.marchenko@gmail.com",
    "user:oleksyy.marchenko@softserveinc.com",
  ]
}
