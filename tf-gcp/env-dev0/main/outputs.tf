output "network_self_link" {
  value = module.network.network_self_link
  description = "The self link of the network"
}
output "subnets" {
  value = {
    vm = module.network.subnets["us-central1/del14-us-central1-vm"].id
  }
  description = "The subnets of the network"
}
