locals {
  gitlab_root_url        = get_env("CI_API_V4_URL", "https://gitlab.com/api/v4")
  gitlab_project_id      = get_env("CI_PROJECT_ID", 53347284)
  gitlab_state_path      = lower(replace(trimprefix(trimprefix(get_original_terragrunt_dir(), get_repo_root()), "/"), "/(/)/", ":"))
  gitlab_backend_address = "${local.gitlab_root_url}/projects/${local.gitlab_project_id}/terraform/state/${local.gitlab_state_path}"
}

remote_state {
  backend = "http"
  config = {
    address        = "${local.gitlab_backend_address}"
    lock_address   = "${local.gitlab_backend_address}/lock"
    unlock_address = "${local.gitlab_backend_address}/lock"
    username       = get_env("GITLAB_USER", "gitlab-ci-token")
    // password       = get_env("CI_JOB_TOKEN") ## Use `export TF_HTTP_PASSWORD` instead
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
}
