terraform {
  required_providers {
    acme = {
      source  = "vancluever/acme"
      version = "~> 2.0"
    }
  }
}

provider "acme" {
  # server_url = "https://acme-staging-v02.api.letsencrypt.org/directory"
  server_url = "https://acme-v02.api.letsencrypt.org/directory"
}

resource "tls_private_key" "private_key" {
  algorithm = "RSA"
}

resource "acme_registration" "reg" {
  account_key_pem = tls_private_key.private_key.private_key_pem
  email_address   = "oleksi.marchenko@gmail.com"
}

resource "acme_certificate" "certificate" {
  account_key_pem           = acme_registration.reg.account_key_pem
  common_name               = "vault-ml.voxsoft.pro"
  subject_alternative_names = ["vault-ml.voxsoft.pro"]

  dns_challenge {
    provider = "gcloud"
    config = {
      GCE_PROJECT = "voxmaster"
    }
  }
}

output "certificate" {
  value = acme_certificate.certificate.certificate_pem
}

output "private_key" {
  value = nonsensitive(acme_certificate.certificate.private_key_pem)
}

