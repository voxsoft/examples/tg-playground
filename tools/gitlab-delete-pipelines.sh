#!/usr/bin/env bash
# This script will delete all GitlabCI Pipelines in specified Gitlab Project

GITLABURL="https://gitlab.com"
PROJECT="45385595"
KEEP=1

_gitlab_pipeline_cleanup() {
  echo GITLAB PIPELINE CLEANUP SCRIPT
  echo DELETING ALL EXCEPT CURRENTLY RUNNING AND THE MOST RECENT PIPELINE
  if [ -z $GITLABURL ]; then
    echo GITLAB_URL:
    read GITLABURL
  fi
  if [ -z $GITLAB_TOKEN ]; then
    echo TOKEN:
    read GITLAB_TOKEN
  fi
  if [ -z $PROJECT ]; then
    echo PROJECT_NUM:
    read PROJECT
  fi

  list=$(curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLABURL/api/v4/projects/$PROJECT/pipelines?per_page=100" | jq -c '.[] | select( .status != "running" )| .id ' | tail -n+$((KEEP + 1)) | grep -v ^$)

  echo removing from $GITLABURL Project number $PROJECT
  while echo $(echo -n "$list" | wc -c) | grep -v ^0$; do
    list=$(curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLABURL/api/v4/projects/$PROJECT/pipelines?per_page=100" | jq -c '.[] | select( .status != "running" )| .id ' | tail -n+$((KEEP + 1)) | grep -v ^$)
    for item in $list; do
      echo -n "| -p $item |"
      curl --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" --request "DELETE" "${GITLABURL}/api/v4/projects/${PROJECT}/pipelines/${item}"
    done
  done
  echo
}

_gitlab_pipeline_cleanup
